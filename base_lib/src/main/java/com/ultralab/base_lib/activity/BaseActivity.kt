package com.ultralab.base_lib.activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.FragmentActivity
import com.alibaba.android.arouter.launcher.ARouter
import com.gyf.immersionbar.BarHide
import com.gyf.immersionbar.ImmersionBar
import com.ultralab.base_lib.R
import com.ultralab.base_lib.base.Presenter
import com.ultralab.base_lib.databinding.ActivityBaseBinding
import com.ultralab.base_lib.tool.AppTool
import com.ultralab.base_lib.tool.getResColor
import com.ultralab.weight_lib.toolbar.CommonToolBar

/**
 *  base
 */
abstract class BaseActivity<VB : ViewDataBinding> : FragmentActivity(),
    Presenter {

    lateinit var mContext: Context
    var toolBar: CommonToolBar? = null

    /**
     * 是否自动调用loadData()方法加载数据，默认为true
     */
    open val autoRefresh = true

    // 布局view
    protected lateinit var bindingView: VB
    var mBaseBinding: ActivityBaseBinding? = null

    //是否显示ToolBar
    open val showToolBar: Boolean = true


    //是否使用自定义底部导航栏
    open val customNavigationBar: Boolean = false

    //底部导航栏颜色
    open fun showNavigationBarColor(): Int = getResColor(R.color.white)

    /**
     * onCreate中方法执行顺序：设置页面布局->初始化页面（子类待实现）->如果autoRefresh为true，加载数据（子类待实现）
     * @param savedInstanceState Bundle
     */
    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        mContext = this
        ARouter.getInstance().inject(this)
        setContentView(getLayoutId())
        initView()
        if (autoRefresh) {
            loadData(true)
        }
    }

    /**
     * 根据是否显示ToolBar加载布局文件
     * @param layoutResID 布局资源文件
     */
    @SuppressLint("NewApi", "SourceLockedOrientationActivity")
    override fun setContentView(@LayoutRes layoutResID: Int) {
        if (showToolBar) {
            mBaseBinding = DataBindingUtil.inflate(
                LayoutInflater.from(this),
                R.layout.activity_base,
                null,
                false
            )
            mBaseBinding!!.llRoot.fitsSystemWindows = true
            bindingView = DataBindingUtil.inflate(layoutInflater, layoutResID, null, false)
            // content
            val params = RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
            bindingView.root.layoutParams = params
            val mContainer = mBaseBinding!!.root.findViewById(R.id.container) as RelativeLayout
            mContainer.addView(bindingView.root)
            window.setContentView(mBaseBinding!!.root)
            mBaseBinding!!.root.addOnLayoutChangeListener { _, _, _, _, _, _, _, _, _ ->
                AppTool.showContentHeight = mBaseBinding!!.root.height
            }
            toolBar = mBaseBinding!!.toolBar
            ImmersionBar.with(this)
                .navigationBarEnable(false)
                .hideBar(BarHide.FLAG_SHOW_BAR).init()

        } else {
            bindingView =
                DataBindingUtil.inflate(LayoutInflater.from(this), layoutResID, null, false)
            window.setContentView(bindingView.root)
            bindingView.root.addOnLayoutChangeListener { _, _, _, _, _, _, _, _, _ ->
                AppTool.showContentHeight = bindingView.root.height
            }
            ImmersionBar.with(this)
                .navigationBarEnable(false)
                .hideBar(BarHide.FLAG_SHOW_BAR).init()
        }
        window.navigationBarColor = showNavigationBarColor()
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    }

    /**
     * 对返回键做特殊处理
     */
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            back()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    /**
     * 点击事件
     * @param v 被点击的view
     */
    override fun onClick(v: View?) {

    }

    /**
     * 设置toolbar
     * @param leftImage 左侧图片
     * @param leftText 左侧文字
     * @param title 标题文字
     * @param rightImage 右侧图片
     * @param rightText 右侧文字
     */
    protected fun setToolBar(
        leftImage: Int,
        leftText: String?,
        title: String?,
        rightImage: Int,
        rightText: String?
    ) {
        if (showToolBar) {
            if (leftImage > 0) {
                toolBar?.setLeftIcon(leftImage)
            }
            if (leftText != null) {
                toolBar?.setLeftTxt(leftText)
            }
            if (title != null) {
                toolBar?.setCenterTitle(title)
            }
            if (rightImage > 0) {
                toolBar?.setRightIcon(rightImage)
            }
            if (rightText != null) {
                toolBar?.setRightTxt(rightText)
            }
            toolBar?.leftListener = { view ->
                onLeftClick(view = view)
            }
            toolBar?.rightListener = { view ->
                onRightClick(view = view)
            }
        }
    }

    /**
     * 设置标题，左边图片右边文字
     * @param leftImg 左侧图片
     * @param titleName 标题文字
     * @param rightName 右侧文字
     */
    protected fun setTitleContent(
        leftImg: Int = R.mipmap.icon_back,
        titleName: String? = null,
        rightName: String? = null
    ) {
        setToolBar(leftImg, null, titleName, -1, rightName)
    }

    /**
     * 标题左右两边为图片
     * @param leftImg 左侧图片
     * @param titleName 标题文字
     * @param rightImg 右侧图片
     */
    protected fun setTitleImg(
        leftImg: Int = R.mipmap.icon_back,
        titleName: String? = null,
        rightImg: Int = -1
    ) {
        setToolBar(leftImg, null, titleName, rightImg, null)
    }

    /**
     * 标题设置左右都为文字
     * @param leftName 左侧文字
     * @param titleName 标题文字
     * @param rightName 右侧文字
     */
    protected fun setTitleNames(
        leftName: String? = null,
        titleName: String? = null,
        rightName: String? = null
    ) {
        setToolBar(-1, leftName, titleName, -1, rightName)
    }

    /**
     * 获取布局文件
     * @return 布局文件
     */
    abstract fun getLayoutId(): Int

    /**
     * 初始化页面
     */
    abstract fun initView()

    /**
     * 返回。toolbar，返回键均会调用。
     */
    open fun back() {
        finish()
    }

    /**
     * 左侧toolbar点击
     * @param view View
     */
    open fun onLeftClick(view: View) {
        back()
    }

    /**
     * 右侧toolbar点击
     * @param view View
     */
    open fun onRightClick(view: View) {

    }

}