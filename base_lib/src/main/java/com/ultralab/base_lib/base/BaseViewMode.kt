package com.ultralab.base_lib.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ultralab.base_lib.tool.SingleLiveEvent
import com.ultralab.base_lib.tool.network.ERROR
import com.ultralab.base_lib.tool.network.ExceptionHandle
import com.ultralab.base_lib.tool.network.PageNetResult
import com.ultralab.base_lib.tool.network.ResponseThrowable
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow


open class BaseViewMode : ViewModel() {
    val defUI: UIChange by lazy { UIChange() }

    /**
     * 所有异步请求都在viewModelScope域中启动，当页面销毁时会自动
     * 调用ViewModel的#onCleared方法取消所有协程
     */
    protected fun launchUI(block: suspend CoroutineScope.() -> Unit) =
        viewModelScope.launch { block() }


    /**
     * 用流的方式进行网络请求
     */
    fun <T> launchFlow(block: suspend () -> T): Flow<T> {
        return flow {
            emit(block())
        }
    }

    /**
     * 获取数据请求结果，不进行响应体过滤
     * @param block 请求体
     * @param error 失败回调
     * @param complete  完成回调（无论成功失败都会调用），默认为空实现
     * @param isShowDialog 是否显示加载框，默认为true
     */
    fun launchGo(
        block: suspend CoroutineScope.() -> Unit,
        error: suspend CoroutineScope.(ResponseThrowable) -> Unit = {
            if (it.code !in ERROR.values().map { error -> error.code }) {
                defUI.toastEvent.postValue(it.errMsg)
            }
        },
        complete: suspend CoroutineScope.() -> Unit = {},
        isShowDialog: Boolean = true
    ) {
        if (isShowDialog) defUI.showDialog.call()
        launchUI {
            handleException(
                withContext(Dispatchers.IO) { block },
                { error(it) },
                {
                    defUI.dismissDialog.call()
                    complete()
                }
            )
        }
    }

    /**
     * 获取数据请求结果，过滤请求结果，其他全抛异常
     * @param block 请求体
     * @param success 成功回调
     * @param error 失败回调，默认为Toast提示错误信息
     * @param complete  完成回调（无论成功失败都会调用），默认为空实现
     * @param isShowDialog 是否显示加载框，默认为false
     */
    fun <T> launchOnlyResult(
        block: suspend CoroutineScope.() -> BaseResponse<T>,
        success: (T) -> Unit,
        error: (ResponseThrowable) -> Unit = {
            if (it.code !in ERROR.values().map { error -> error.code }) {
                defUI.toastEvent.postValue(it.errMsg)
            }
        },
        complete: () -> Unit = {},
        isShowDialog: Boolean = false
    ) {
        if (isShowDialog) defUI.showDialog.call()
        launchUI {
            handleException(
                {
                    withContext(Dispatchers.IO) {
                        block().let {
                            if (it.isSuccess()) it.data()
                            else throw ResponseThrowable(it.code(), it.msg())
                        }
                    }.also { success(it) }
                },
                { error(it) },
                {
                    if (isShowDialog) defUI.dismissDialog.call()
                    complete()
                }
            )
        }
    }

    /**
     * 获取分页数据请求结果，过滤请求结果，其他全抛异常
     * @param block 请求体
     * @param success 成功回调
     * @param error 失败回调，默认为Toast提示错误信息
     * @param complete  完成回调（无论成功失败都会调用），默认为空实现
     * @param isShowDialog 是否显示加载框，默认为false
     */
    fun <T> launchPageResult(
        block: suspend CoroutineScope.() -> BaseResponse<T>,
        success: (data: T, existSurplusData: Boolean) -> Unit,
        error: (ResponseThrowable) -> Unit = {
            defUI.toastEvent.postValue(it.errMsg)
        },
        complete: () -> Unit = {},
        isShowDialog: Boolean = false
    ) {
        if (isShowDialog) defUI.showDialog.call()
        launchUI {
            handleException(
                {
                    withContext(Dispatchers.IO) {
                        block().let {
                            if (it.isSuccess()) it
                            else throw ResponseThrowable(it.code(), it.msg())
                        }
                    }.also {
                        success(
                            it.data(),
                            if (it is PageNetResult<*>) it.existSurplusData() else false
                        )
                    }
                },
                { error(it) },
                {
                    if (isShowDialog) defUI.dismissDialog.call()
                    complete()
                }
            )
        }
    }

    /**
     * 异常统一处理
     */
    private suspend fun handleException(
        block: suspend CoroutineScope.() -> Unit,
        error: suspend CoroutineScope.(ResponseThrowable) -> Unit,
        complete: suspend CoroutineScope.() -> Unit
    ) {
        coroutineScope {
            try {
                block()
            } catch (e: Throwable) {
                error(ExceptionHandle.handleException(e))
            } finally {
                complete()
            }
        }
    }

    /**
     * UI事件
     */
    inner class UIChange {
        val showDialog by lazy { SingleLiveEvent<String>() }
        val dismissDialog by lazy { SingleLiveEvent<Void>() }
        val toastEvent by lazy { SingleLiveEvent<String>() }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }
}