package com.ultralab.base_lib.tool.network

import com.google.gson.GsonBuilder
import com.ultralab.base_lib.tool.network.interceptor.LoggingInterceptor
import com.ultralab.base_lib.tool.print
import okhttp3.ConnectionPool
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


/**
 * @ClassName: RetrofitClient
 * @Description: 基础网络请求
 * @Author: jason@leanktech.com
 * @Date: 2021/9/13 15:12
 */
object RetrofitClient {

    fun getRetrofit(client: OkHttpClient, baseUrl: String): Retrofit {
        val gson = GsonBuilder()
            .setLenient()
            .create()
        return Retrofit.Builder()
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(baseUrl)
            .build()
    }


    fun getOkHttpClient(): OkHttpClient =
        OkHttpClient.Builder()
            .connectTimeout(20L, TimeUnit.SECONDS)//连接超时时间
            .writeTimeout(60L, TimeUnit.SECONDS)//写入超时时间
            .connectionPool(ConnectionPool(8, 15, TimeUnit.SECONDS))//连接池
            .addInterceptor(LoggingInterceptor(logger = { message -> message.print("-NET-") }).apply {
                level = LoggingInterceptor.Level.BODY
            })//日志拦截器
            .build()
}