package com.ultralab.base_lib.tool





/**
 * App项目配置信息
 */
class AppConfig {
    companion object {
        /**
         * 是否强制切换为正式版
         *      true：强制切换为发布版
         *      false：debug包为测试版，release包为发布版
         */
        const val FORCE_RELEASE = false
        /**
         * AppName
         */
        const val APP_NAME = "egg_android"
        /**
         * 测试地址
         */
        const val BASE_URL_FOR_TEST = "http://dev.doudouxiaoshuo.cn"

        /**
         * 正式地址
         */
        const val BASE_URL_FOR_RELEASE = "https://api.doudouxiaoshuo.cn"

        /**
         * 测试地址_用户
         */
        const val App_URL = "/app/"
        /**
         * 测试地址_用户
         */
        const val USER_URL = "/user/"
        /**
         * 测试地址_养鸡场
         */
        const val CLOCK_URL = "/chook/"
        /**
         * 测试地址_收益
         */
        const val PROFIT_URL = "/profit/"
        /**
         * 测试地址_小说
         */
        const val READER_URL = "/book/"
        /**
         * 数据库名称
         */
        const val DB_NAME: String = "eggreader_client.db"
        /**
         * 隐私政策地址
         */
        const val PRIVACY_URL: String = "https://img.doudouxiaoshuo.cn/egg_privacy.html"
        /**
         * 用户协议地址
         */
        const val AGREEMENT_URL: String = "https://img.doudouxiaoshuo.cn/egg_user.html"

        /**
         * 攻略H5地址
         * */
        const val INTRODUCTION_URL : String = "https://www.baidu.com/?tn=90544950_hao_pg"

        /**
         * 后台打包固定渠道号
         * */
        const val CHANNEL_FIXATION_ONE = "20000001"

        /**
         * 后台打包固定渠道号
         * */
        const val CHANNEL_FIXATION_TWO = "20000002"

        /**
         * 蛋读授权码
         * */
        const val EGG_AUTHORIZATION_CODE = "1627634999917"

        /**
         * 今日头条
         * */
        const val JIN_RI_TOU_TIAO_AD_MASTER_ID = "5154305"
        /**
         * 广点通
         * */
        const val GDT_AD_MASTER_ID = "1111690958"
        /**
         * tui
         * */
        const val TUI_AD_MASTER_ID = "82992"
        /**
         * 华为
         * */
        const val HW_AD_MASTER_ID = "103896923"
        /**
         * 快手
         * */
        const val KS_AD_MASTER_ID = "526200003"
        /**
         * 百度
         * */
        const val BAIDU_AD_MASTER_ID = "fb782b4b"

    }
}

/**
 * Activity请求码
 * 注意：
 * RequestCode必须大于0
 * 当前设计分4位。第1位：所属模块；第2-3位：请求内容；第4位：返回结果使用
 * 1:Home模块2:用户模块;3:养鸡模块;4:阅读模块
 */
class ActivityRequestCode {
    companion object {
        /**
         * 使用相机
         */
        const val USE_CAMERA = 0x1_01_0
        /**
         * 图片选择
         */
        const val PIC_CHOOSE = 0x2_01_0
        /**
         * 图片裁剪
         */
        const val PIC_CROP = 0x2_02_0
        /**
         * 添加收货地址
         */
        const val ADD_ADDRESS = 0x2_03_0
        /**
         * 绑定账户
         */
        const val BINDING_ACCOUNT = 0x2_04_0
        /**
         * 选择订单地址
         */
        const val ORDER_CHOOSE_ADDRESS = 0x3_01_0
    }
}

/**
 * Activity结果码
 * 注意：
 * ResultCode必须大于0
 * 当前设计分4位。第1位：所属模块；第2-3位：请求内容；第4位：返回结果使用
 * 1:Home模块2:用户模块;3:养鸡模块;4:阅读模块
 */
class ActivityResultCode {
    companion object {
        /**
         * 图片选择结果
         */
        const val PIC_CHOOSE_RESULT = 0x2_01_1
        /**
         * 图片裁剪结果
         */
        const val PIC_CROP_RESULT = 0x2_02_1
        /**
         * 添加收货地址
         */
        const val ADD_ADDRESS_RESULT = 0x2_03_1
        /**
         * 绑定账户
         */
        const val BINDING_ACCOUNT_RESULT= 0x2_04_1
        /**
         * 选择订单地址
         */
        const val ORDER_CHOOSE_ADDRESS_RESULT = 0x3_01_1

        /**
         * 销毁小红点
         * */
        const val REMOVE_RED_DOTS = 0x4_01_1
        /**
         * 反馈小红点
         * */
        const val FEED_BACK_RED_DOT = 0x4_01_2
    }
}

/**
 * 主页Tab类型
 */
class HomeTab {
    companion object {
        /**
         * 小说
         */
        const val TYPE_BOOKSHELF = 0
        /**
         * 书架
         */
        const val TYPE_BOOKCASE = 1
        /**
         * 福利页
         */
        const val TYPE_WELFARE = 2
        /**
         * 养鸡场
         */
        const val TYPE_FARM = 3
        /**
         * 我的
         */
        const val TYPE_MINE = 4


        /**
         * 背景颜色-农场选中
         */
        const val COLOR_BG_FARM_SELECT = 0xFFFFE2C5.toInt()
        /**
         * 背景颜色-普通选中
         */
        const val COLOR_BG_NORMAL_SELECT = 0xFFFFFFFF.toInt()
        /**
         * Tab字体颜色-农场选中
         */
        const val COLOR_TXT_FARM_SELECT = 0xFF8B572A.toInt()
        /**
         * Tab字体颜色-农场未选中
         */
        const val COLOR_TXT_FARM_UNSELECT = 0xFFC59970.toInt()
        /**
         * Tab字体颜色-普通选中
         */
        const val COLOR_TXT_NORMAL_SELECT = 0xFFFF9B00.toInt()
        /**
         * Tab字体颜色-普通未选中
         */
        const val COLOR_TXT_NORMAL_UNSELECT = 0xFFB3B3B3.toInt()
    }
}

/**
 * 养鸡场页操作类型
 */
class BoonOperation {
    companion object {
        /**
         * 打开礼品兑换页面
         */
        const val OPEN_EXCHANGE_GIFT = 1
        /**
         * 打开领取饲料页面
         */
        const val OPEN_GET_FEED_TASK = 2
        /**
         * 打开阅读有礼页面
         */
        const val OPEN_GIFT_FOR_READING = 3
        /**
         * 打开养鸡有礼页面
         */
        const val OPEN_GIFT_FOR_FEEDING = 4
        /**
         * 打开签到页面
         */
        const val OPEN_ACCOUNT_SIGN = 5
        /**
         * 打开绑定有礼任务
         */
        const val OPEN_GIFT_FOR_BINDING = 6
        /**
         * 打开收集有礼任务
         */
        const val OPEN_GIFT_FOR_COLLECTING = 7
        /**
         * 打开邀请有礼任务
         */
        const val OPEN_GIFT_FOR_INVITE_FRIEND = 8
        /**
         * 打开清理任务
         */
        const val OPEN_FEED_TASK_FOR_CLEAR_SHIT = 9
        /**
         * 打开喝水任务
         */
        const val OPEN_FEED_TASK_FOR_DRINK_WATER = 10
        /**
         * 打开玩耍任务
         */
        const val OPEN_FEED_TASK_FOR_PLAY_WITH_CHICKEN= 11
        /**
         * 播放清理鸡场动画
         */
        const val START_CLEAR_SHIT_ANIMATION = 12

        /**
         * 播放喝水动画
         */
        const val START_CHICK_DRINK_WATER_ANIMATION = 13

        /**
         * 与鸡互动动画
         */
        const val START_INTERACTIVE_ANIMATION_WITH_CHICKEN = 14

        /**
         * 发生打开我的好友列表事件
         */
        const val START_OPEN_MY_FRIEND = 15
    }
}


/**
 * item类型
 */
class ItemType {
    companion object {
        /**
         * 顶部
         */
        const val HEADER = 0
        /**
         * 普通
         */
        const val NORMAL = 1
        /**
         * 底部
         */
        const val FOOTER = 2
        /**
         * 空
         */
        const val EMPTY = 3
    }
}

/**
 * 时间类型长度
 */
class TimeMsLength {
    companion object {
        /**
         * 秒
         */
        const val ONE_SECOND = 1000L
        /**
         * 分钟
         */
        const val ONE_MINUTE = 60000L
        /**
         * 小时
         */
        const val ONE_HOUR = 3600000L
        /**
         * 天
         */
        const val ONE_DAY = 86400000L
        /**
         * 月
         */
        const val ONE_MONTH = 2592000000L
        /**
         * 年
         */
        const val ONE_YEAR = 31536000000L
    }
}

/**
 * 展示时间类型
 */
class ShowTimeType {
    companion object {
        /**
         * 秒
         */
        const val SECOND = 0b1111110
        /**
         * 分钟
         */
        const val MINUTE = 0b1111101
        /**
         * 小时
         */
        const val HOUR = 0b1111011
        /**
         * 天
         */
        const val DAY = 0b1110111
        /**
         * 月
         */
        const val MONTH = 0b1101111
        /**
         * 年
         */
        const val YEAR = 0b1011111
        /**
         * 全部
         */
        const val ALL = 0b1000000
        /**
         * 只显示最大
         */
        const val ONLY_MAX = 0b0111111
    }
}

/**
 * vip类型
 */
class VipMember {
    companion object {
        /**
         * 普通用户
         */
        const val NORMAL = 0
        /**
         * vip包月
         */
        const val VIP_MONTHLY = 1
        /**
         * vip包季
         */
        const val VIP_QUARTERLY = 2
        /**
         * vip包年
         */
        const val VIP_YEAR = 3
    }
}


/**
 * 收货地址页面类型
 */
class AddressPageType {
    companion object {
        /**
         * 选择
         */
        const val CHOOSE = 0
        /**
         * 编辑
         */
        const val EDIT = 1
    }
}


/**
 * 通知相关
 */
class FarmNotificationInfo {
    companion object {
        /**
         * 有效时间
         */
        const val VALIDITY_TIME = TimeMsLength.ONE_DAY
    }
}

/**
 * 小鸡状态
 */
class ChickenStateInfo {
    companion object {
        /**
         * 健康小鸡
         */
        const val HEALTHY = 1
        /**
         * 生病小鸡
         */
        const val UN_HEALTHY = 2
        /**
         * 吃饲料中...
         */
        const val EATING = 3
    }
}

/**
 * 养鸡场背景音乐状态
 */
class ChickenFarmMusicState {
    companion object {
        /**
         * 停止
         */
        const val STOP = 0
        /**
         * 播放
         */
        const val PLAY = 1
        /**
         * 暂停
         */
        const val PAUSE = 2
    }
}

/**
 * 养鸡场引导状态
 */
class ChickenFarmGuideState{
    companion object {
        /**
         * 不需要引导
         */
        const val GUIDE_0 = 0

        /**
         * 引导第一步欢迎弹窗
         */
        const val GUIDE_1 = 1

        /**
         * 引导第二步 连续三个引导图切换
         */
        const val GUIDE_2 = 2
        /**
         * 显示喂饲料小手
         */
        const val GUIDE_3 = 3
        /**
         * 第一次喂饲料赠送鸡蛋弹窗
         */
        const val GUIDE_4 = 4
        /**
         * 领取鸡蛋以后引导去填写收货地址
         */
        const val GUIDE_5 = 5
        /**
         * 填写收货地址或没填写显示去兑换小手
         */
        const val GUIDE_6 = 6
        /**
         * 养鸡场引导结束
         */
        const val GUIDE_7 = 7
    }
}

/**
 * 首页推荐列表类型
 */
class FrequencyRecommendPositionId {
    companion object {
        //经典推荐ID
        const val CLASSIC_SUGGESTION_ID = 0
        //本周热门
        const val POPULAR_THIS_WEEK = 1
        //四图卡片自定义榜
        const val FOUR_PICTURE_CARD = 3
        //2*5卡片自定义榜
        const val TWO_FIVE_PICTURE_CARD = 2
        //1+4卡片自定义榜
        const val ONE_FOUR_PICTURE_CARD = 4
        //2*4卡片自定义榜
        const val TWO_FOUR_PICTURE_CARD = 5

    }
}

/**
 * 书籍加入书架状态
 */
class BookAddBookshelfState {
    companion object {
        //未加入书架
        const val NOT_ADDED_TO_BOOKSHELF = 0
        //已加入书架
        const val ADDED_TO_BOOKSHELF = 1

    }
}

/**
 * App HomeTab配置
 */
class AppHomeTabHint {
    companion object {

        //默认不隐藏
        const val DEFAULT = -1
        //隐藏书架TAB
        const val HINT_BOOKCASE_TAB = 0
        //隐藏养鸡场TAB
        const val HINT_CHICKEN_TAB = 1
    }
}

class JumpConfig{
    companion object {
        /** 跳转 id -- 阅读器  */
        const val JUMP_READER = 1

        /** 养鸡场 */
        const val JUMP_CHICKEN = 2

        /** 小说 */
        const val JUMP_BOOK_MALL = 3

        /** 书架 */
        const val JUMP_BOOK_RACK = 4

        /** 会员充值页 */
        const val JUMP_VIP_RECHARGE = 5

        /** 免邮卡 */
        const val JUMP_FREE_EMAIL_CARD = 6

        /** 我的零钱 */
        const val JUMP_ME_MONEY = 7

        /** 我的收获地址 */
        const val JUMP_ME_ADDRESS = 8

        /** 领饲料浮层 */
        const val JUMP_COLLAR_FEED = 9

        /** 兑换礼品浮层 */
        const val JUMP_GIFT_EXCHANGE = 10

        /** 账号管理 */
        const val JUMP_ACCOUNT_MANAGE = 11

        /** H5页面 */
        const val JUMP_WEB = 12
    }
}

class LogTag{
    companion object{
        /**
         * ADTag
         */
        const val AD = "egg_ad"

        /**
         * UMTag
         */
        const val UM = "egg_um"

        /**
         * SLSTag
         */
        const val SLS = "egg_sls"
    }
}