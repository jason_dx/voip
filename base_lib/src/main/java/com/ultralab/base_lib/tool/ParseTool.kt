package com.ultralab.base_lib.tool

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.CharacterStyle
import android.text.style.ForegroundColorSpan
import okhttp3.MediaType
import okhttp3.RequestBody
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern


/**
 * px转换sp
 */
fun Float.px2sp(context: Context = ActivityMgr.getContext()): Int =
    (this / context.applicationContext.resources.displayMetrics.scaledDensity + 0.5f).toInt()

fun Int.px2sp(context: Context = ActivityMgr.getContext()): Int =
    (this / context.applicationContext.resources.displayMetrics.scaledDensity + 0.5f).toInt()

/**
 * sp转换px
 */
fun Float.sp2px(context: Context = ActivityMgr.getContext()): Int =
    (this * context.applicationContext.resources.displayMetrics.scaledDensity + 0.5f).toInt()

fun Int.sp2px(context: Context = ActivityMgr.getContext()): Int =
    (this * context.applicationContext.resources.displayMetrics.scaledDensity + 0.5f).toInt()

/**
 * px转换dp
 */
fun Float.px2dp(context: Context = ActivityMgr.getContext()): Int =
    (this / context.applicationContext.resources.displayMetrics.density + 0.5f).toInt()

fun Int.px2dp(context: Context = ActivityMgr.getContext()): Int =
    (this / context.applicationContext.resources.displayMetrics.density + 0.5f).toInt()

/**
 * dp转换px
 */
fun Float.dp2px(context: Context = ActivityMgr.getContext()): Int =
    (this * context.applicationContext.resources.displayMetrics.density + 0.5f).toInt()

fun Int.dp2px(context: Context = ActivityMgr.getContext()): Int =
    (this * context.applicationContext.resources.displayMetrics.density + 0.5f).toInt()


/**
 * 将double转换成百分比
 */
fun Double.toPercent(): String = DecimalFormat("0.00%").format(this)

/**
 * 将时间戳转换成时间字符串
 */
@SuppressLint("SimpleDateFormat")
fun Long.toTime(regex: String = "yyyy-MM-dd HH:mm:ss"): String {
    return try {
        val format = SimpleDateFormat(regex)
        format.format(Date(this))
    } catch (e: Exception) {
        ""
    }
}

/**
 * 毫秒时间戳 转换成日期
 * @param regex 格式化时间格式
 * */
@SuppressLint("SimpleDateFormat")
fun String.strToTime(regex: String = "yyyy-MM-dd HH:mm:ss"): String? {
    val simpleDateFormat = SimpleDateFormat(regex)
    //如果它本来就是long类型的,则不用写这一步
    val date = Date(this.toLong().orZero())
    return simpleDateFormat.format(date)
}

/**
 * 将请求参数Map转换为请求体
 * @receiver 存放请求参数的Map
 * @param existPublicParm 是否有公共参数，默认存在公共参数
 * @return RequestBody
 */
fun HashMap<String, Any?>.toRequestBody(existPublicParm: Boolean = true): RequestBody {
    if (existPublicParm) {
        if (!this.containsKey("app_name")) {
            this["app_name"] = AppConfig.APP_NAME
        }
        //if (!this.containsKey("appVer")) {
        //    this["appVer"] = DeployBean.appVer?.replace(".dev", "")
        //}
        //if (!this.containsKey("channelId")) {
        //    this["channelId"] = DeployBean.appChannelNumber
        //}
        //if (!this.containsKey("deviId")) {
        //    this["deviId"] = DeployBean.deviId
        //}
        //if (!this.containsKey("pushId") && DeployBean.pushId.isNotEmpty()) {
        //    this["pushId"] = DeployBean.pushId
        //}
        //if (!this.containsKey("userId")) {
        //    this["userId"] = AccountBean.accountId
        //}
        //if (!this.containsKey("site")) {
        //    this["site"] = AccountBean.userSite
        //}
    }
    return RequestBody.create(
        MediaType.parse("application/json; charset=utf-8"), this.toJson() ?: ""
    )
}


private fun toSeconds(time: Long): Long = time / 1000L

private fun toMinutes(time: Long): Long = toSeconds(time) / 60L

private fun toHours(time: Long): Long = toMinutes(time) / 60L

private fun toDays(time: Long): Long = toHours(time) / 24L

private fun toMonths(time: Long): Long = toDays(time) / 30L

private fun toYears(time: Long): Long = toMonths(time) / 365L


/**
 * 判断当前时间戳是否是今天
 */
fun Long.isToday(): Boolean {
    val currentTime = Date(this)
    val todayStart = Calendar.getInstance()
    todayStart[Calendar.HOUR_OF_DAY] = 0
    todayStart[Calendar.MINUTE] = 0
    todayStart[Calendar.SECOND] = 0
    todayStart[Calendar.MILLISECOND] = 0
    val todayBegin = todayStart.time
    return !currentTime.before(todayBegin)
}

/**
 * 是上午还是下午
 * 0=am 1=pm
 */
fun Long.isAmOrPm(): Boolean {
    val mCalendar = Calendar.getInstance()
    mCalendar.timeInMillis = this
    val hour = mCalendar.get(Calendar.HOUR)
    val apm = mCalendar.get(Calendar.AM_PM)
    return apm != 0
}

/**
 * 将颜色去除透明度
 * @receiver 透明颜色
 * @param bgColor 背景色
 * @return 不透明颜色
 */
fun Int.colorRemoveTransparent(bgColor: Int = Color.WHITE): Int {
    val showColorTransparent = this.ushr(24)
    val showColorRed = this.shl(8).ushr(24)
    val showColorGreen = this.shl(16).ushr(24)
    val showColorBlue = this.shl(24).ushr(24)
    val bgColorRed = bgColor.shl(8).ushr(24)
    val bgColorGreen = bgColor.shl(16).ushr(24)
    val bgColorBlue = bgColor.shl(24).ushr(24)
    val finalColorRed =
        ((1L * showColorRed * showColorTransparent + 1L * bgColorRed * (255 - showColorTransparent)) / 256).toInt()
    val finalColorGreen =
        ((1L * showColorGreen * showColorTransparent + 1L * bgColorGreen * (255 - showColorTransparent)) / 256).toInt()
    val finalColorBlue =
        ((1L * showColorBlue * showColorTransparent + 1L * bgColorBlue * (255 - showColorTransparent)) / 256).toInt()
    val finalColor = 255.shl(24) + finalColorRed.shl(16) + finalColorGreen.shl(8) + finalColorBlue
    return finalColor
}

/**
 * 替换文字高亮显示
 * 返回关键字和原本字符串拼接之后的
 * */
fun matcherSearchContent(text: String?, keyword1: Array<String>): SpannableStringBuilder? {
    val keyword = arrayOfNulls<String>(keyword1.size)
    System.arraycopy(keyword1, 0, keyword, 0, keyword1.size)
    val spannable = SpannableStringBuilder(text)
    var span: CharacterStyle?
    var wordReg: String
    for (i in keyword.indices) {
        var key = ""
        //  处理通配符问题
        if (keyword[i]!!.contains("*") || keyword[i]!!.contains("(") || keyword[i]!!.contains(")")) {
            val chars = keyword[i]!!.toCharArray()
            for (k in chars.indices) {
                key = if (chars[k] == '*' || chars[k] == '(' || chars[k] == ')') {
                    key + "\\" + chars[k].toString()
                } else {
                    key + chars[k].toString()
                }
            }
            keyword[i] = key
        }
        wordReg = "(?i)" + keyword[i] //忽略字母大小写
        val pattern = Pattern.compile(wordReg)
        val matcher = pattern.matcher(text.toString())
        while (matcher.find()) {
            span = ForegroundColorSpan(Color.parseColor("#FF8000"))
            spannable.setSpan(span, matcher.start(), matcher.end(), Spannable.SPAN_MARK_MARK)
        }
    }
    return spannable
}

/**
 *
 * double 转 double ，四舍五入保留两位小数。
 */
private fun toDoubleTwoFormat(value: Double): Double {
    return Math.round((value + 0.0000001) * 100).toDouble() / 100
}

/**
 * double 转 string ，四舍五入保留两位小数。
 *  */
fun toStringTwoFormat(value: Double): String? {
    return String.format("%.2f", toDoubleTwoFormat(value))
}

/**
 * double 转 string ，四舍五入保留两位小数。
 *  */
fun toStringOneFormat(value: Double): String? {
    return String.format("%.1f", toDoubleTwoFormat(value))
}

/**
 * 获取当前货币 符号
 * */
fun getSymbol(): String {
    val currency = Currency.getInstance(Locale.getDefault())
    return currency.symbol
}

/**
 * 金额为分的格式
 */
const val CURRENCY_FEN_REGEX = "\\-?[0-9]+"


/**
 * Long 为null 的扩展函数
 */
fun Long?.orZero(): Long {
    return this ?: 0L
}

/**
 * int 为null 的扩展函数
 */
fun Int?.orZero(): Int {
    return this ?: 0
}

/**
 * 判断是否为空 或者 为null的情况
 * null："null"
 * null: "(null)"
 * null: "<null>"
 */
fun String?.isNotNullEmpty(): Boolean {
    return this != null && this.isNotEmpty() && this.toLowerCase(Locale.ENGLISH) != "null" && this.toLowerCase(
        Locale.ENGLISH
    ) != "(null)" && this.toLowerCase(Locale.ENGLISH) != "<null>"
}


/**
 * 两个时间相差距离多少天多少小时多少分多少秒
 * ["year", "month", "weekOfYear", "day", "hour", "minute", "second", "nanosecond"]
 */
fun getContrastTime(orderDateTime: Long): Long {
    val diff = java.lang.Long.valueOf(orderDateTime.toString()) - getTimeStampBeforeXXDay(
        date = Date(),
        30
    )
    val day = diff / (24 * 60 * 60 * 1000)
    //val hour = diff / (60 * 60 * 1000) - day * 24
    //val min = diff / (60 * 1000) - day * 24 * 60 - hour * 60
    //val sec = diff / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60
    //val year = day / 365
    //val month = day / 30
    //val week = day / 7
    if (day > 0) {
        return day
    }
    return 0
}

/**
 * 秒专车对应时间
 */
fun Long.millisToRegex(): String {
    fun unitFormat(long: Long): String {
        return if (long in 0..9) {
            "0$long"
        } else {
            "$long"
        }
    }

    val hours = this / 3600
    var second = this % 3600
    val minutes = second / 60
    second %= 60
    return if (hours > 0) {
        unitFormat(hours) + ":" + unitFormat(minutes) + ";" + unitFormat(second)
    } else {
        unitFormat(minutes) + ":" + unitFormat(second)
    }
}

/**
 * 获取 多少天 后的 时间戳
 * */
fun getTimeStampBeforeXXDay(date: Date?, beforeDays: Int): Long {
    val cal = Calendar.getInstance()
    cal.time = date
    cal[Calendar.HOUR_OF_DAY] = 0
    cal[Calendar.SECOND] = 0
    cal[Calendar.MINUTE] = 0
    cal[Calendar.MILLISECOND] = 0
    //把日期往后增加一天,整数  往后推,负数往前移动
    cal.add(Calendar.DATE, -beforeDays)
    return cal.timeInMillis
}

/**
 * 获取assets 内容
 */
fun getAssetsJson(fileName: String?, context: Context): String {
    //将json数据变成字符串
    val stringBuilder = StringBuilder()
    try {
        //获取assets资源管理器
        val assetManager = context.assets
        //通过管理器打开文件并读取
        val bf = BufferedReader(
            InputStreamReader(
                assetManager.open(fileName!!)
            )
        )
        var line: String?
        while (bf.readLine().also { line = it } != null) {
            stringBuilder.append(line)
        }
    } catch (e: IOException) {
        e.printStackTrace()
    }
    return stringBuilder.toString()
}