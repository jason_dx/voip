package com.ultralab.base_lib.tool

import android.annotation.SuppressLint
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.net.Uri
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.ultralab.base_lib.tool.image.ImageLoadConfig
import com.ultralab.base_lib.tool.image.load
import java.io.File
import java.io.IOException


/**
 * 加载图片资源
 * @param v ImageView
 * @param path 网络资源路径
 * @param filePath 本地资源路径
 * @param uri uri
 * @param loadType 加载类型
 * @param cropType 裁剪类型
 * @param priority 加载优先级
 * @param placeholder 占位图
 * @param corner 圆角角度
 */
@BindingAdapter(
    value = ["path", "filePath", "uri", "loadType", "cropType", "priority", "placeholder", "corner"],
    requireAll = false
)
fun setImgBinding(
    v: ImageView,
    path: String?,
    filePath: String?,
    uri: Uri?,
    loadType: ImageLoadConfig.LoadType?,
    cropType: ImageLoadConfig.CropType?,
    priority: ImageLoadConfig.Priority?,
    placeholder: Drawable?,
    corner: Int?
) {
    @Suppress("IMPLICIT_CAST_TO_ANY") val loadData = when {
        !path.isNullOrEmpty() -> path
        !filePath.isNullOrEmpty() -> File(filePath)
        uri != null -> uri
        else -> ""
    }
    val config = ImageLoadConfig.Builder()
        .setLoadType(
            loadType
                ?: if (corner != null && corner > 0) ImageLoadConfig.LoadType.CORNER else ImageLoadConfig.LoadType.NORMAL
        )
        .setCropType(cropType ?: ImageLoadConfig.CropType.CENTER_CROP)
        .setPriority(priority ?: ImageLoadConfig.Priority.NORMAL)
        .placeholder(placeholder).errorImage(placeholder)
        .setCorner(
            corner
                ?: (if (loadType != null && loadType == ImageLoadConfig.LoadType.CORNER) 3 else 0)
        ).build()
    v.load(loadData, config)
}

@SuppressLint("SetTextI18n")
@BindingAdapter(value = ["showTime"])
fun setShowTime(v: TextView, time: Long) {
    if (time > 0) {
        val today = time.isToday()
        if (today) {
            //是今天
            val amOrPm = time.isAmOrPm()
            val apmStr = if (amOrPm) {
                "PM"
            } else {
                "AM"
            }
            v.text = "$apmStr ${time.toString().strToTime(regex = "HH:mm")}"
        } else {
            //不是今天
            v.text = "${time.toString().strToTime(regex = "MM-dd HH:mm")}"
        }
    }
}

@BindingAdapter(value = ["callAnswerTime"])
fun setCallAnswerTime(v: TextView, time: Long) {
    if (time > 0) {
        v.text = "time: ${time.millisToRegex()}"
    } else {
        v.text = "No Answer"
    }
}

@BindingAdapter(value = ["loadAssetsIma"])
fun setLoadAssetsIma(v: ImageView, url: String) {
    val assetManager = ActivityMgr.getContext().assets;
    try {
        val fileName = "flag/$url"
        //filename是assets目录下的图片名
        val inputStream = assetManager.open(fileName)
        val bitmap = BitmapFactory.decodeStream(inputStream)
        v.setImageBitmap(bitmap)
    } catch (e: IOException) {
        e.printStackTrace()
    }
}

@BindingAdapter(value = ["showAssetsIma"])
fun setShowAssetsIma(v: ImageView, str: String) {
    if (str.isNotNullEmpty()){
        val split = str.split(",")
        split[0].let {
            val assetManager = ActivityMgr.getContext().assets
            try {
                val fileName = "flag/$it"
                //filename是assets目录下的图片名
                val inputStream = assetManager.open(fileName)
                val bitmap = BitmapFactory.decodeStream(inputStream)
                v.setImageBitmap(bitmap)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }
}
@BindingAdapter(value = ["showCountryName"])
fun setShowCountryName(v: TextView, str: String) {
    if (str.isNotNullEmpty()) {
        val split = str.split(",")
        split[1].let {
            v.text = it
        }
    }
}
@BindingAdapter(value = ["showCountryCode"])
fun setShowCountryCode(v: TextView, str: String) {
    if (str.isNotNullEmpty()) {
        val split = str.split(",")
        split[2].let {
            v.text = "+$it"
        }
    }
}
@BindingAdapter(value = ["width", "height"], requireAll = false)
fun setLayout(view: View, width: Int?, height: Int?) {
    view.layoutParams = view.layoutParams.apply {
        height?.let {
            this.height = it
        }
        width?.let {
            this.width = it
        }
    }
}



