package com.ultralab.base_lib.tool.image

/**
 * 图片加载监听
 */
interface ImageLoadingListener {
    /**
     * 成功回调
     */
    fun onSuccess()
    /**
     * 失败回调
     */
    fun onError()
}