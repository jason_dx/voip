package com.ultralab.base_lib.tool.image

import android.widget.ImageView

/**
 * 图片加载抽象类
 */
abstract class ImageLoader protected constructor() {

    /**
     * 加载图片资源
     * @param imageView ImageView
     * @param loadData 图片资源
     * @param config 图片加载配置
     * @param listener 普通加载回调监听
     * @param bitmapListener Bitmap回调监听
     */
    abstract fun load(imageView: ImageView, loadData: Any, config: ImageLoadConfig = defaultConfig(), listener: ImageLoadingListener? = null, bitmapListener: BitmapLoadingListener? = null)

    /**
     * 清除缓存
     */
    abstract fun clearCache()

    companion object {
        /**
         * 获取GlideLoader实现类
         * @return GlideLoader实现类
         */
        fun getGlideInstance(): ImageLoader {
            return GlideLoader()
        }
    }

}