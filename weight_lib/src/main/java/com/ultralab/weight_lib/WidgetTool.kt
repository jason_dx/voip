package com.ultralab.weight_lib

import android.content.Context
import android.os.Build
import android.util.DisplayMetrics
import android.view.WindowManager

object WidgetTool {
    /**
     * dp值转换为px值
     * @param context 上下文对象
     * @param dpValue dp值
     * @return px值
     */
    fun dp2px(context: Context, dpValue: Int): Int =
        (dpValue * context.applicationContext.resources.displayMetrics.density + 0.5f).toInt()

    /**
     * px值转换为dp值
     * @param context 上下文对象
     * @param pxValue px值
     * @return dp值
     */
    fun px2dp(context: Context, pxValue: Int): Int =
        (pxValue / context.applicationContext.resources.displayMetrics.density + 0.5f).toInt()

    /**
     * sp值转换为px值
     * @param context 上下文对象
     * @param spValue sp值
     * @return px值
     */
    fun sp2px(context: Context, spValue: Int): Int =
        (spValue * context.applicationContext.resources.displayMetrics.scaledDensity + 0.5f).toInt()

    /**
     * px值转换为sp值
     * @param context 上下文对象
     * @param pxValue px值
     * @return sp值
     */
    fun px2sp(context: Context, pxValue: Int): Int =
        (pxValue / context.applicationContext.resources.displayMetrics.scaledDensity + 0.5f).toInt()

    /**
     * 获取屏幕宽度
     * @param context 上下文对象
     * @return 屏幕宽度，单位PX
     */
    fun getScreenWidth(context: Context): Int {
        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val mDisplayMetrics = DisplayMetrics()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            context.display?.getRealMetrics(mDisplayMetrics)
        } else {
            wm.defaultDisplay.getMetrics(mDisplayMetrics)
        }
        return mDisplayMetrics.widthPixels
    }

    /**
     * 获取屏幕高度
     * @param context 上下文对象
     * @return 屏幕高度，单位PX
     */
    fun getScreenHeight(context: Context): Int {
        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val mDisplayMetrics = DisplayMetrics()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            context.display?.getRealMetrics(mDisplayMetrics)
        } else {
            wm.defaultDisplay.getMetrics(mDisplayMetrics)
        }
        return mDisplayMetrics.heightPixels
    }

    /**
     * 通过反射的方式获取状态栏高度
     * @param context 上下文对象
     * @return 状态栏高度
     */
    fun getStatusBarHeight(context: Context): Int {
        try {
            val c = Class.forName("com.android.internal.R\$dimen")
            val obj = c.newInstance()
            val field = c.getField("status_bar_height")
            val x = Integer.parseInt(field.get(obj).toString())
            return context.resources.getDimensionPixelSize(x)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return 0
    }

}