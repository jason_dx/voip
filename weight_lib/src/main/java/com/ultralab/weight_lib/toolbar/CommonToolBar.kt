package com.ultralab.weight_lib.toolbar

import android.content.Context
import android.os.Build
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.ultralab.weight_lib.R
import com.ultralab.weight_lib.WidgetTool

/**
 * 自定义ToolBar
 * 分为左中右三部分
 * 左右分别可以显示icon和文字，可以设置点击监听
 * 中间可显示文字
 */
class CommonToolBar @JvmOverloads constructor(
    private val mContext: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(mContext, attrs, defStyleAttr), View.OnClickListener {

    //------------------------------公共属性开始------------------------------
    lateinit var leftListener: (view: View) -> Unit
    lateinit var rightListener: (view: View) -> Unit
    //------------------------------公共属性结束------------------------------

    private lateinit var mInflater: LayoutInflater
    private var mView: View? = null

    private lateinit var toolBarBgView: LinearLayout

    //内部组件-左
    private lateinit var leftPart: LinearLayout
    private lateinit var leftText: TextView
    private lateinit var leftImage: ImageView

    //内部组件-中
    private lateinit var centerPart: LinearLayout

    //标题
    private lateinit var centerTitle: TextView

    //内部组件-右
    private lateinit var rightPart: LinearLayout
    private lateinit var rightText: TextView
    private lateinit var rightImage: ImageView

    //是否有上边距
    var topSpace = true
        set(topSpace) {
            field = topSpace
            if (statusBarHeight != null) {
                if (topSpace) {
                    statusBarHeight?.visibility = View.VISIBLE
                    //获取到状态栏的高度
                    val statusHeight = WidgetTool.getStatusBarHeight(mContext)
                    //动态的设置隐藏布局的高度
                    val params = statusBarHeight?.layoutParams as LinearLayout.LayoutParams
                    params.height = statusHeight
                    statusBarHeight?.layoutParams = params
                } else {
                    statusBarHeight?.visibility = View.GONE
                }
            }
        }

    //顶部间隔
    private var statusBarHeight: LinearLayout? = null

    init {
        initView()
        if (attrs != null) {
            val a = mContext.obtainStyledAttributes(
                attrs,
                R.styleable.CommonToolBar, defStyleAttr, 0
            )
            topSpace = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                a.getBoolean(R.styleable.CommonToolBar_topSpace, false)
            } else {
                a.getBoolean(R.styleable.CommonToolBar_topSpace, true)
            }
            if (topSpace) {
                statusBarHeight?.visibility = View.VISIBLE
                //获取到状态栏的高度
                val statusHeight = WidgetTool.getStatusBarHeight(mContext)
                //动态的设置隐藏布局的高度
                val params = statusBarHeight?.layoutParams as LinearLayout.LayoutParams
                params.height = statusHeight
                statusBarHeight?.layoutParams = params
            }

            val leftTxt = a.getText(R.styleable.CommonToolBar_leftButtonTxt)
            setLeftTxt(leftTxt)

            val leftIcon = a.getDrawable(R.styleable.CommonToolBar_leftButtonIcon)
            if (leftIcon != null) {
                leftImage.visibility = View.VISIBLE
                leftImage.setImageDrawable(leftIcon)
            }

            val rightTxt = a.getText(R.styleable.CommonToolBar_rightButtonTxt)
            setRightTxt(rightTxt)

            val rightIcon = a.getDrawable(R.styleable.CommonToolBar_rightButtonIcon)
            if (rightIcon != null) {
                rightImage.visibility = View.VISIBLE
                rightImage.setImageDrawable(rightIcon)
            }

            val centerTitle = a.getText(R.styleable.CommonToolBar_centerTitle)
            a.recycle()
            setCenterTitle(centerTitle)
        }
    }

    /**
     * 初始化自定义view
     */
    private fun initView() {
        if (mView == null) {
            mInflater = LayoutInflater.from(mContext)
            mView = mInflater.inflate(R.layout.widget_toolbar, null)

            toolBarBgView = mView!!.findViewById(R.id.tool_bar_layout) as LinearLayout

            leftPart = mView!!.findViewById(R.id.left_part)
            leftText = mView!!.findViewById(R.id.left_text)
            leftImage = mView!!.findViewById(R.id.left_image) as ImageView

            centerPart = mView!!.findViewById(R.id.center_part)
            centerTitle = mView!!.findViewById(R.id.center_title)

            rightPart = mView!!.findViewById(R.id.right_part)
            rightText = mView!!.findViewById(R.id.right_text)
            rightImage = mView!!.findViewById(R.id.right_image) as ImageView

            leftPart.setOnClickListener(this)
            rightPart.setOnClickListener(this)

            statusBarHeight = mView!!.findViewById(R.id.status_bar_height)


            val lp = LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                Gravity.CENTER_HORIZONTAL
            )

            addView(mView, lp)
        }
    }


    //------------------------------公共方法开始------------------------------
    /**
     * 设置左侧文字，并根据是否为null控制显隐
     * @param leftTxt 左侧文字内容
     */
    fun setLeftTxt(leftTxt: CharSequence?) {
        if (leftTxt != null) {
            leftText.visibility = View.VISIBLE
            leftText.text = leftTxt
        } else {
            leftText.visibility = View.GONE
        }
    }

    /**
     * 设置左侧icon图标res，并进行显示
     * @param res 左侧图标res
     */
    fun setLeftIcon(res: Int) {
        leftImage.visibility = View.VISIBLE
        leftImage.setImageResource(res)
    }

    /**
     * 设置右侧文字，并根据是否为null控制显隐
     * @param rightTxt 右侧文字内容
     */
    fun setRightTxt(rightTxt: CharSequence?) {
        if (rightTxt != null) {
            rightText.visibility = View.VISIBLE
            rightText.text = rightTxt
        } else {
            rightText.visibility = View.GONE
        }
    }

    /**
     * 设置右侧icon图标res，并进行显示
     * @param res 左侧图标res
     */
    fun setRightIcon(res: Int) {
        rightImage.visibility = View.VISIBLE
        rightImage.setImageResource(res)
    }

    /**
     * 设置标题文字，并根据是否为null控制显隐
     * @param title 标题文字内容
     */
    fun setCenterTitle(title: CharSequence?) {
        if (title != null) {
            centerTitle.visibility = View.VISIBLE
            centerTitle.text = title
        } else {
            centerTitle.text = ""
        }
    }

    /**
     * 设置toobar背景
     */
    fun setToolBarBg(bgColor: Int) {
        if (bgColor != 0) {
            toolBarBgView.setBackgroundColor(bgColor)
        }
    }
    /**
     * 设置toobar背景
     */
    fun setToolBarTitleBg(bgColor: Int) {
        if (bgColor != 0) {
            centerTitle.setTextColor(bgColor)
        }
    }
    //------------------------------公共方法结束------------------------------

    override fun onClick(view: View) {
        when (view.id) {
            R.id.left_part -> if (::leftListener.isInitialized && (leftText.visibility == VISIBLE && leftText.text.toString()
                    .isNotEmpty() || leftImage.visibility == VISIBLE)
            ) {
                leftListener.invoke(view)
            }
            R.id.right_part -> if (::rightListener.isInitialized && (rightText.visibility == VISIBLE && rightText.text.toString()
                    .isNotEmpty() || rightImage.visibility == VISIBLE)
            ) {
                rightListener.invoke(view)
            }
        }
    }

}
