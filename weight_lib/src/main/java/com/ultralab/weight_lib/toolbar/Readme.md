使用说明：
CommonToolBar：
    自定义ToolBar。
    自适应状态栏，分为左中右三部分。
    中部可设置标题，不可点击。
    左右两侧可设置文字，图片，均可设置点击监听。
    常用xml属性：
        leftButtonTxt:左侧按钮文字
        leftButtonIcon:左侧按钮图标
        rightButtonTxt:右侧按钮文字
        rightButtonIcon:右侧按钮图标
        centerTitle:标题文字
        topSpace:是否有上边距去适应状态栏
    常用属性：
        leftListener:左侧点击监听
        rightListener:右侧点击监听
    常用方法：
        setLeftTxt(leftTxt: CharSequence?):设置左侧按钮文字
        setLeftIcon(res: Int):设置左侧按钮图标
        setRightTxt(rightTxt: CharSequence?):设置右侧按钮文字
        setRightIcon(res: Int):设置右侧按钮图标
        setCenterTitle(title: CharSequence?):设置标题文字