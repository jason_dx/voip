package com.ultralab.ultracall.db

import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.ultralab.base_lib.tool.print
import com.ultralab.base_lib.tool.toJson
import com.ultralab.base_lib.tool.toMap
import com.ultralab.ultracall.FirStoreTable
import com.ultralab.ultracall.FirStoreTableCollection
import com.ultralab.ultracall.StoreToKey
import com.ultralab.ultracall.activity.bean.DialListBean
import com.ultralab.ultracall.local.AccountBean
import com.ultralab.ultracall.tools.dbCreateTable
import com.ultralab.ultracall.tools.getDbJsonData

/**
 * @ClassName: DbManger
 * @Description: firStore 存储工具
 * @Author: jason@leanktech.com
 * @Date: 2021/9/3 15:15
 */
object DbManger {

    /** db 对象*/
    fun getFireStore() = Firebase.firestore

    fun createUserTable(bean: DialListBean) {
        val user = bean.toMap()
        user["temporaryId"] = AccountBean.userId.toLong()
        val data = dbCreateTable().apply {
            put(StoreToKey.KEY_CALL_RECORDS, mutableListOf(user))
        }
        getFireStore().collection(FirStoreTableCollection.TABLE_USERS)
            .document(FirStoreTable.TAB_USER)
            .set(data)
            .addOnSuccessListener {
                "DocumentSnapshot added".print()
            }
            .addOnFailureListener { e ->
                "Error adding document $e".print()
            }
    }

    fun upDataUser(saveBean: DialListBean) {
        val saveList = mutableListOf<HashMap<String,Any?>>()
        //先查找
        getFireStore().collection(FirStoreTableCollection.TABLE_USERS)
            .document(FirStoreTable.TAB_USER)
            .get()
            .addOnSuccessListener {
                "${it.data?.toJson()} ".print()

                //新数据存第一个位置
                saveList.add(saveBean.toMap())
                if (it.data?.toJson() != null){
                    val dbJsonData =
                        it.data?.getDbJsonData(StoreToKey.KEY_CALL_RECORDS, DialListBean::class.java)
                    //旧数据存在新集合据里面
                    dbJsonData?.forEach { itemBean->
                        saveList.add(itemBean.toMap())
                    }
                }
                //对数据进行put
                val data = dbCreateTable().apply {
                    put(StoreToKey.KEY_CALL_RECORDS, saveList)
                }
                //调用set方法覆盖
                getFireStore().collection(FirStoreTableCollection.TABLE_USERS)
                    .document(FirStoreTable.TAB_USER)
                    .set(data)
                    .addOnSuccessListener {
                        "DocumentSnapshot added".print()
                    }
                    .addOnFailureListener { e ->
                        "Error adding document $e".print()
                    }
            }
    }


    fun getUserTableData() {
        getFireStore().collection(FirStoreTableCollection.TABLE_USERS)
            .document(FirStoreTable.TAB_USER)
            .get()
            .addOnSuccessListener {
                "${it.data?.toJson()} ".print()
            }.addOnFailureListener {
                "Error get document $it".print()
            }.addOnCanceledListener {
                "Error get Canceled ".print()
            }.addOnCompleteListener { task ->
                //如果当前没有数据会走addOnCompleteListener方法，会走到addOnSuccessListener 但query是空的
                task.addOnFailureListener {
                    "task Error get document $it".print()
                }.addOnCanceledListener {
                    "task Error get Canceled ".print()
                }.addOnSuccessListener {
                    "task success ".print()
                }
            }
    }
}