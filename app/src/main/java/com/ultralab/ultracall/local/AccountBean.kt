package com.ultralab.ultracall.local

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.ultralab.base_lib.BR
import com.ultralab.base_lib.tool.keyvalue.MmkvKeyValueMgr
import java.io.Serializable

/**
 * @ClassName: AccountBean
 * @Description: 本地持久化
 * @Author: jason@leanktech.com
 * @Date: 2021/9/3 15:50
 */
object AccountBean : BaseObservable(), Serializable {

    /**
     * 用户id
     */
    @set:Bindable
    var userId: String = MmkvKeyValueMgr.get<String>("userId", "") ?: ""
        @Bindable get
        set(userId) {
            field = userId
            userId.let { MmkvKeyValueMgr.put("userId", it) }
            notifyPropertyChanged(BR.userId)
        }

    /**
     * 用户id
     */
    @set:Bindable
    var deviceId: String = MmkvKeyValueMgr.get<String>("deviceId", "") ?: ""
        @Bindable get
        set(deviceId) {
            field = deviceId
            deviceId.let { MmkvKeyValueMgr.put("deviceId", it) }
            notifyPropertyChanged(BR.deviceId)
        }

    /**
     * 是否登录过
     */
    @set:Bindable
    var checkLogin: Boolean = MmkvKeyValueMgr.get<Boolean>("checkLogin", false) ?: false
        @Bindable get
        set(checkLogin) {
            field = checkLogin
            checkLogin.let { MmkvKeyValueMgr.put("checkLogin", it) }
            notifyPropertyChanged(BR.checkLogin)
        }

    /**
     * 是否第一次进入app
     */
    @set:Bindable
    var firstGoApp: Boolean = MmkvKeyValueMgr.get<Boolean>("firstGoApp", false) ?: false
        @Bindable get
        set(firstGoApp) {
            field = firstGoApp
            firstGoApp.let { MmkvKeyValueMgr.put("firstGoApp", it) }
            notifyPropertyChanged(BR.firstGoApp)
        }

    /**
     * 广告累积，电话总计时间
     */
    @set:Bindable
    var adCredits: Int = MmkvKeyValueMgr.get<Int>("adCredits", 1000) ?: 1000
        @Bindable get
        set(adConsumptionTimes) {
            field = adConsumptionTimes
            adConsumptionTimes.let { MmkvKeyValueMgr.put("adCredits", it) }
            notifyPropertyChanged(BR.adCredits)
        }

    /**
     * 拨打电话消耗时间
     */
    @set:Bindable
    var timeRemainingMin: Int = MmkvKeyValueMgr.get<Int>("timeRemainingMin", 0) ?: 0
        @Bindable get
        set(timeRemainingMin) {
            field = timeRemainingMin
            timeRemainingMin.let { MmkvKeyValueMgr.put("adCredits", it) }
            notifyPropertyChanged(BR.timeRemainingMin)
        }

    /**
     * 单次消耗
     */
    @set:Bindable
    var singleConsumption: Int = MmkvKeyValueMgr.get<Int>("singleConsumption", 10) ?: 10
        @Bindable get
        set(singleConsumption) {
            field = singleConsumption
            singleConsumption.let { MmkvKeyValueMgr.put("singleConsumption", it) }
            notifyPropertyChanged(BR.timeRemainingMin)
        }

    /**
     * 存储上一次选择的地区
     */
    @set:Bindable
    var lastSelectedCountry: String = MmkvKeyValueMgr.get<String>("lastSelectedCountry", "US.png,United States,1") ?: "US.png,United States,1"
        @Bindable get
        set(lastSelectedCountry) {
            field = lastSelectedCountry
            lastSelectedCountry.let { MmkvKeyValueMgr.put("lastSelectedCountry", it) }
            notifyPropertyChanged(BR.lastSelectedCountry)
        }

    /**
     * 存储token
     */
    @set:Bindable
    var twilioToken: String = MmkvKeyValueMgr.get<String>("twilioToken", "") ?: ""
        @Bindable get
        set(twilioToken) {
            field = twilioToken
            twilioToken.let { MmkvKeyValueMgr.put("twilioToken", it) }
            notifyPropertyChanged(BR.twilioToken)
        }
    /**
     * 今天是否签到
     * 存时间戳
     * */
    var todayCheckInString: Long = MmkvKeyValueMgr.get("todayCheckInString", 0L) ?: 0L
        set(value) {
            field = value
            MmkvKeyValueMgr.put("todayCheckInString", value)
        }
}

