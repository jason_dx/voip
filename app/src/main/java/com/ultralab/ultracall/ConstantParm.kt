package com.ultralab.ultracall

/**
 * 参数配置页面
 */

/**
 * activity for on result code
 */
class ActivityRes {
    companion object {
        const val MIC_PERMISSION_REQUEST_CODE = 0x101
    }
}

/**
 * google ad id
 */
class GoogleAd {
    companion object {
        const val GOOGLE_AD_TEST_ID = "ca-app-pub-3940256099942544/5224354917"

        //app open
        const val GOOGLE_AD_APP_OPEN_ID = "ca-app-pub-6394006065968726/4187555897"

        //开屏测试id
        const val GOOGLE_AD_SPLASH_ID = "ca-app-pub-6394006065968726/7289697112"

        //原生
        const val GOOGLE_AD_NATIVE_ID = "ca-app-pub-6394006065968726/4754230787"
    }
}

class NetWorkUrl {
    companion object {
        const val BASE_URL = "https://quickstart-9547-dev.twil.io/"

        //获取token
        const val ACCESS_TOKEN = "access-token"
    }
}

class CallParameter {
    companion object {
        //呼叫
        const val ACTION_INCOMING_CALL = "ACTION_INCOMING_CALL"

        //取消
        const val ACTION_CANCEL_CALL = "ACTION_CANCEL_CALL"

        //fcm
        const val ACTION_FCM_TOKEN = "ACTION_FCM_TOKEN"
        const val INCOMING_CALL_INVITE = "INCOMING_CALL_INVITE"
        const val CANCELLED_CALL_INVITE = "CANCELLED_CALL_INVITE"
        const val INCOMING_CALL_NOTIFICATION_ID = "INCOMING_CALL_NOTIFICATION_ID"

        //来电通知
        const val ACTION_INCOMING_CALL_NOTIFICATION = "ACTION_INCOMING_CALL_NOTIFICATION"

        //接收来电
        const val ACTION_ACCEPT = "ACTION_ACCEPT"

        const val ACTION_REJECT = "ACTION_REJECT"
        const val CALL_SID_KEY = "CALL_SID"
        const val VOICE_CHANNEL_HIGH_IMPORTANCE = "notification-channel-high-importance"
        const val VOICE_CHANNEL_LOW_IMPORTANCE = "notification-channel-low-importance"

    }
}

/**
 * 透传到呼叫页面参数内容
 */
class ActivityCallParameter {
    companion object {
        const val CALL_AREA_CODE = "areaCode"
        const val CALL_PHONE_NUMBER = "phoneNumber"
        const val CALL_AREA_NAME = "areaName"
        const val CALL_AREA_IMAGE = "areaImage"
        const val CALL_IS_SAVE_DB = "isSaveDb"
    }
}

/**
 * live event bus
 */
class LiveEventData {
    companion object {
        const val INIT_STATE = "initState"
        const val UPDATE_LIST = "up_date_list"

        //单次消耗
        const val CONSUMPTION_INTEGRAL = "consumption_integral"
    }
}

/**
 * 数据库表名
 *  */
class FirStoreTableCollection {
    companion object {
        //通话记录
        const val TABLE_USERS = "users"
    }
}

/**
 * 数据库表子表名
 */
class FirStoreTable {
    companion object {
        //通话记录
        const val TAB_USER = "user"
    }
}

/**
 * 数据表 关键字
 * 等于实际你存入store的key
 */
class StoreToKey {
    companion object {
        const val KEY_CALL_RECORDS = "callRecords"
    }
}

/**
 * 路由地址
 */
class ARoutePath {
    companion object {
        private const val HOME_PATH = "/home"
        private const val SPLASH_PATH = "/splash"

        const val ROUTE_SPLASH_PATH = "$SPLASH_PATH/splash"
        const val H5_PATH = "$HOME_PATH/h5Activity"
    }
}

/**
 * 网页地址
 */
class UrlPath {
    companion object {
        //隐私政策
        const val PRIVACY_POLICY = "https://ultra-call.web.app/privacy.html"

        //用户条款
        const val USER_AGREEMENT = "https://ultra-call.web.app/terms.html"

        //关于我们
        const val ABOUT = "https://ultra-call.web.app/"
    }
}