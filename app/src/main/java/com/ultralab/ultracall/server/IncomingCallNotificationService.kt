package com.ultralab.ultracall.server

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.*
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import androidx.core.app.NotificationCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.twilio.voice.CallInvite
import com.ultralab.base_lib.tool.print
import com.ultralab.ultracall.CallParameter
import com.ultralab.ultracall.R
import com.ultralab.ultracall.activity.home.activity.HomeActivity

/**
 * @ClassName: IncomingCallNotificationService
 * @Description: 绑定通知
 * @Author: jason@leanktech.com
 * @Date: 2021/8/24 10:48
 */
class IncomingCallNotificationService : Service() {

    override fun onBind(intent: Intent?): IBinder? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val action = intent!!.action

        if (action != null) {
            val callInvite: CallInvite = intent.getParcelableExtra(CallParameter.INCOMING_CALL_INVITE)!!
            val notificationId = intent.getIntExtra(CallParameter.INCOMING_CALL_NOTIFICATION_ID, 0)
            when (action) {
                CallParameter.ACTION_INCOMING_CALL -> handleIncomingCall(callInvite, notificationId)
                CallParameter.ACTION_ACCEPT -> accept(callInvite, notificationId)
                CallParameter.ACTION_REJECT -> reject(callInvite)
                CallParameter.ACTION_CANCEL_CALL -> handleCancelledCall(intent)
                else -> {
                }
            }
        }
        return START_NOT_STICKY
    }

    @SuppressLint("UnspecifiedImmutableFlag")
    private fun createNotification(
        callInvite: CallInvite,
        notificationId: Int,
        channelImportance: Int
    ): Notification? {
        val intent = Intent(this, HomeActivity::class.java)
        intent.action = CallParameter.ACTION_INCOMING_CALL_NOTIFICATION
        intent.putExtra(CallParameter.INCOMING_CALL_NOTIFICATION_ID, notificationId)
        intent.putExtra(CallParameter.INCOMING_CALL_INVITE, callInvite)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(
            this,
            notificationId,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
        /*
         * Pass the notification id and call sid to use as an identifier to cancel the
         * notification later
         */
        val extras = Bundle()
        extras.putString(CallParameter.CALL_SID_KEY, callInvite.callSid)
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            buildNotification(
                callInvite.from + " is calling.",
                pendingIntent,
                extras,
                callInvite,
                notificationId,
                createChannel(channelImportance)
            )
        } else {
            NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(callInvite.from + " is calling.")
                .setAutoCancel(true)
                .setExtras(extras)
                .setContentIntent(pendingIntent)
                .setGroup("test_app_notification")
                .setColor(Color.rgb(214, 10, 37)).build()
        }
    }

    /**
     * Build a notification.
     *
     * @param text          the text of the notification
     * @param pendingIntent the body, pending intent for the notification
     * @param extras        extras passed with the notification
     * @return the builder
     */
    @SuppressLint("UnspecifiedImmutableFlag")
    @TargetApi(Build.VERSION_CODES.O)
    private fun buildNotification(
        text: String, pendingIntent: PendingIntent, extras: Bundle,
        callInvite: CallInvite,
        notificationId: Int,
        channelId: String
    ): Notification? {
        val rejectIntent = Intent(applicationContext, IncomingCallNotificationService::class.java)
        rejectIntent.action = CallParameter.ACTION_REJECT
        rejectIntent.putExtra(CallParameter.INCOMING_CALL_INVITE, callInvite)
        rejectIntent.putExtra(CallParameter.INCOMING_CALL_NOTIFICATION_ID, notificationId)
        val piRejectIntent = PendingIntent.getService(
            applicationContext,
            0,
            rejectIntent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
        val acceptIntent = Intent(applicationContext, IncomingCallNotificationService::class.java)
        acceptIntent.action = CallParameter.ACTION_ACCEPT
        acceptIntent.putExtra(CallParameter.INCOMING_CALL_INVITE, callInvite)
        acceptIntent.putExtra(CallParameter.INCOMING_CALL_NOTIFICATION_ID, notificationId)
        val piAcceptIntent = PendingIntent.getService(
            applicationContext,
            0,
            acceptIntent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
        val builder: Notification.Builder = Notification.Builder(
            applicationContext, channelId
        )
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle(getString(R.string.app_name))
            .setContentText(text)
            .setCategory(Notification.CATEGORY_CALL)
            .setExtras(extras)
            .setAutoCancel(true)
            .addAction(R.mipmap.ic_launcher, getString(R.string.decline), piRejectIntent)
            .addAction(R.mipmap.ic_launcher, getString(R.string.answer), piAcceptIntent)
            .setFullScreenIntent(pendingIntent, true)
        return builder.build()
    }

    @TargetApi(Build.VERSION_CODES.O)
    private fun createChannel(channelImportance: Int): String {
        var callInviteChannel = NotificationChannel(
            CallParameter.VOICE_CHANNEL_HIGH_IMPORTANCE,
            "Primary Voice Channel", NotificationManager.IMPORTANCE_HIGH
        )
        var channelId: String = CallParameter.VOICE_CHANNEL_HIGH_IMPORTANCE
        if (channelImportance == NotificationManager.IMPORTANCE_LOW) {
            callInviteChannel = NotificationChannel(
                CallParameter.VOICE_CHANNEL_LOW_IMPORTANCE,
                "Primary Voice Channel", NotificationManager.IMPORTANCE_LOW
            )
            channelId = CallParameter.VOICE_CHANNEL_LOW_IMPORTANCE
        }
        callInviteChannel.lightColor = Color.GREEN
        callInviteChannel.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(callInviteChannel)
        return channelId
    }

    private fun accept(callInvite: CallInvite, notificationId: Int) {
        endForeground()
        val activeCallIntent = Intent(this, HomeActivity::class.java)
        activeCallIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
        activeCallIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        activeCallIntent.putExtra(CallParameter.INCOMING_CALL_INVITE, callInvite)
        activeCallIntent.putExtra(CallParameter.INCOMING_CALL_NOTIFICATION_ID, notificationId)
        activeCallIntent.action = CallParameter.ACTION_ACCEPT
        startActivity(activeCallIntent)
    }

    private fun reject(callInvite: CallInvite) {
        endForeground()
        callInvite.reject(applicationContext)
    }

    private fun handleCancelledCall(intent: Intent) {
        endForeground()
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    private fun handleIncomingCall(callInvite: CallInvite, notificationId: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            setCallInProgressNotification(callInvite, notificationId)
        }
        sendCallInviteToActivity(callInvite, notificationId)
    }

    private fun endForeground() {
        stopForeground(true)
    }

    @TargetApi(Build.VERSION_CODES.O)
    private fun setCallInProgressNotification(callInvite: CallInvite, notificationId: Int) {
        if (isAppVisible()) {
            "setCallInProgressNotification - app is visible.".print()
            startForeground(
                notificationId,
                createNotification(callInvite, notificationId, NotificationManager.IMPORTANCE_LOW)
            )
        } else {
            "setCallInProgressNotification - app is NOT visible.".print()
            startForeground(
                notificationId,
                createNotification(callInvite, notificationId, NotificationManager.IMPORTANCE_HIGH)
            )
        }
    }

    /*
     * Send the CallInvite to the MainActivity. Start the activity if it is not running already.
     */
    private fun sendCallInviteToActivity(callInvite: CallInvite, notificationId: Int) {
        if (Build.VERSION.SDK_INT >= 29 && !isAppVisible()) {
            return
        }
        val intent = Intent(this, HomeActivity::class.java)
        intent.action = CallParameter.ACTION_INCOMING_CALL
        intent.putExtra(CallParameter.INCOMING_CALL_NOTIFICATION_ID, notificationId)
        intent.putExtra(CallParameter.INCOMING_CALL_INVITE, callInvite)
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        this.startActivity(intent)
    }

    private fun isAppVisible(): Boolean {
        return ProcessLifecycleOwner
            .get()
            .lifecycle
            .currentState
            .isAtLeast(Lifecycle.State.STARTED)
    }
}