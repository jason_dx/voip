package com.ultralab.ultracall.fcm

import android.content.Intent
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.twilio.voice.*
import com.ultralab.base_lib.tool.printError
import com.ultralab.ultracall.CallParameter
import com.ultralab.ultracall.server.IncomingCallNotificationService

/**
 * @ClassName: VoiceFirebaseMessagingService
 * @Description: 注册google fcm
 * @Author: jason@leanktech.com
 * @Date: 2021/8/24 10:38
 */
class VoiceFirebaseMessagingService: FirebaseMessagingService() {

    override fun onCreate() {
        super.onCreate()
    }

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        "Received onMessageReceived()".printError()
        "Bundle data: ${remoteMessage.data}".printError()
        "From: remoteMessage.from".printError()

        // Check if message contains a data payload.
        if (remoteMessage.data.isNotEmpty()) {
            val valid = Voice.handleMessage(this, remoteMessage.data, object : MessageListener {
                override fun onCallInvite(callInvite: CallInvite) {
                    val notificationId = System.currentTimeMillis().toInt()
                    handleInvite(callInvite, notificationId)
                }

                override fun onCancelledCallInvite(
                    cancelledCallInvite: CancelledCallInvite,
                    callException: CallException?
                ) {
                    handleCanceledCallInvite(cancelledCallInvite)
                }
            })
            if (!valid) {
                "The message was not a valid Twilio Voice SDK payload: ${ remoteMessage.data}".printError()
            }
        }
    }

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
        val intent = Intent(CallParameter.ACTION_FCM_TOKEN)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }


    private fun handleInvite(callInvite: CallInvite, notificationId: Int) {
        val intent = Intent(this, IncomingCallNotificationService::class.java)
        intent.action = CallParameter.ACTION_INCOMING_CALL
        intent.putExtra(CallParameter.INCOMING_CALL_NOTIFICATION_ID, notificationId)
        intent.putExtra(CallParameter.INCOMING_CALL_INVITE, callInvite)
        startService(intent)
    }

    private fun handleCanceledCallInvite(cancelledCallInvite: CancelledCallInvite) {
        val intent = Intent(this, IncomingCallNotificationService::class.java)
        intent.action = CallParameter.ACTION_CANCEL_CALL
        intent.putExtra(CallParameter.CANCELLED_CALL_INVITE, cancelledCallInvite)
        startService(intent)
    }




}