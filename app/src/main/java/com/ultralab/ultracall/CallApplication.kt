package com.ultralab.ultracall

import android.content.Context
import android.os.Environment
import androidx.multidex.MultiDexApplication
import com.alibaba.android.arouter.launcher.ARouter
import com.getkeepsafe.relinker.ReLinker
import com.google.android.gms.ads.MobileAds
import com.orhanobut.logger.LogLevel
import com.orhanobut.logger.Logger
import com.tencent.mmkv.MMKV
import com.twilio.voice.LogModule
import com.twilio.voice.Voice
import com.ultralab.base_lib.tool.ActivityMgr
import com.ultralab.base_lib.tool.CrashHandler
import com.ultralab.base_lib.tool.print
import org.koin.android.ext.android.startKoin
import org.koin.android.logger.AndroidLogger

/**
 * @ClassName: MyApp
 * @Description: 程序入口
 * @Author: jason@leanktech.com
 * @Date: 2021/8/20 15:51
 */
class CallApplication : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()

        initActivityManager()
        initKoin()
        initARouter()
        initWrapMMKV(this)
        initLog()
        crashHandlerInit()
        initTwilio()
        //        val configuration = RequestConfiguration.Builder()
//            .setTestDeviceIds(listOf("b9570a7e-7af0-4670-83a8-d01cd69fba71")).build()
//        MobileAds.setRequestConfiguration(configuration)
        MobileAds.initialize(
            this
        ) {
            "ads init status$it".print()
        }
    }

    /**
     * 初始化twilio log
     */
    private fun initTwilio() {
        Voice.setLogLevel(com.twilio.voice.LogLevel.ALL)
        Voice.setModuleLogLevel(LogModule.CORE, com.twilio.voice.LogLevel.ALL)
    }

    /**
     * 初始化MMKV
     * @param context Context
     */
    private fun initWrapMMKV(context: Context) {
        try {
            initMMKV(context)
        } catch (e: UnsatisfiedLinkError) {
            initMMKV(context) { s: String? -> ReLinker.loadLibrary(context, s) }
        }
    }

    private fun initMMKV(context: Context, loader: MMKV.LibLoader? = null) {
        try {
            val root = context.filesDir.absolutePath + "/mmkv"
            MMKV.initialize(root, loader)
        } catch (e: NullPointerException) {
            var cacheDir = context.cacheDir
            if (cacheDir == null) {
                cacheDir = Environment.getRootDirectory()
            }
            MMKV.initialize("$cacheDir/mmkv", loader)
        }
    }

    /**
     * 初始化日志
     */
    private fun initLog() {
        if (BuildConfig.DEBUG) {
            Logger.init(this.packageName).methodOffset(2).methodCount(1).hideThreadInfo()
        } else {
            Logger.init().logLevel(LogLevel.NONE)
        }
    }

    /**
     * 初始化Activity管理器
     */
    private fun initActivityManager() {
        ActivityMgr.init(this)
    }

    /**
     * 初始化Koin
     */
    private fun initKoin() {
        startKoin(applicationContext, appModule, logger = AndroidLogger())
    }

    /**
     * 本地崩溃日志采集
     */
    private fun crashHandlerInit() {
        if (!BuildConfig.DEBUG) {
            CrashHandler.getInstance().init(this)
        }
    }

    private fun initARouter() {
        if (BuildConfig.DEBUG){
            // 打印日志
            ARouter.openLog()
            // 开启调试模式(如果在InstantRun模式下运行，必须开启调试模式！线上版本需要关闭,否则有安全风险)
            ARouter.openDebug()
        }
        // 尽可能早，推荐在Application中初始化
        ARouter.init(this)
    }

    override fun onTerminate() {
        super.onTerminate()
        //aRouter释放资源
        ARouter.getInstance().destroy()
    }
}