package com.ultralab.ultracall.conversation

import com.ultralab.ultracall.activity.bean.CallErrorBean

/**
 * @ClassName: CallNumberBack
 * @Description: call number dsl call back
 * @Author: jason@leanktech.com
 * @Date: 2021/8/24 14:34
 */
class CallNumberBack {

    lateinit var build: Builder

    fun setCallBack(call: Builder.() -> Unit) {
        this.build = Builder().also(call)
    }

    inner class Builder {
        //异常处理
        internal var callError: ((CallErrorBean) -> Unit)? = null

        //接听
        internal var callAnswer: ((Boolean) -> Unit)? = null

        //断开链接
        internal var callDisconnected: (((CallErrorBean?) -> Unit))? = null

        //通话质量变动
        internal var callQualityWarningsChanged: (((CallErrorBean) -> Unit))? = null

        //刷新UI
        internal var callRefresh: ((Boolean) -> Unit)? = null

        //显示通话ui
        internal var callSetUI: ((Boolean) -> Unit)? = null

        fun callError(action: (CallErrorBean) -> Unit) {
            callError = action
        }

        fun callAnswer(action: (Boolean) -> Unit) {
            callAnswer = action
        }

        fun callDisconnected(action: (CallErrorBean?) -> Unit) {
            callDisconnected = action
        }

        fun callQualityWarningsChanged(action: (CallErrorBean) -> Unit) {
            callQualityWarningsChanged = action
        }

        fun callRefresh(action: (Boolean) -> Unit) {
            callRefresh = action
        }

        fun callSetUI(action: (Boolean) -> Unit) {
            callSetUI = action
        }
    }
}