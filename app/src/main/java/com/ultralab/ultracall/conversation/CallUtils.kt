package com.ultralab.ultracall.conversation

import android.app.AlertDialog
import android.app.NotificationManager
import android.content.*
import android.media.AudioManager
import androidx.fragment.app.FragmentActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.firebase.messaging.FirebaseMessaging
import com.jeremyliao.liveeventbus.LiveEventBus
import com.twilio.audioswitch.AudioDevice
import com.twilio.audioswitch.AudioSwitch
import com.twilio.voice.*
import com.ultralab.base_lib.tool.*
import com.ultralab.ultracall.CallParameter
import com.ultralab.ultracall.LiveEventData
import com.ultralab.ultracall.activity.bean.CallErrorBean
import com.ultralab.ultracall.local.AccountBean
import com.ultralab.ultracall.server.IncomingCallNotificationService
import java.util.*

/**
 * @ClassName: CallUtils
 * @Description: 加载打电话工具类
 * @Author: jason@leanktech.com
 * @Date: 2021/8/24 13:58
 */
object CallUtils {

    //twilio 是否初始化
    private var checkTwilioInit = false

    //call 是否已激活
    private var checkCallActivate = false

    //通知
    private var notificationManager: NotificationManager? = null

    //
    private var activeCall: Call? = null
    private var audioSwitch: AudioSwitch? = null
    //广播是否已注册过
    private var isReceiverRegistered = false
    private var voiceBroadcastReceiver: BroadcastReceiver? = null
    private var savedVolumeControlStream = 0
    private var activeCallInvite: CallInvite? = null
    private var activeCallNotificationId = 0
    private var registrationListener = registrationListener()
    private var mActivity: FragmentActivity? = null

    //回调
    private var callBacks: CallNumberBack? = null


    fun initCall(context: FragmentActivity) {
        mActivity = context
        notificationManager =
            context.getSystemService(FragmentActivity.NOTIFICATION_SERVICE) as NotificationManager
        //设置广播接收器，以通知FCM令牌更新或此活动中的来电邀请。
        voiceBroadcastReceiver = VoiceBroadcastReceiver()
        registerReceiver()
        audioSwitch = AudioSwitch(context.applicationContext)
        savedVolumeControlStream = mActivity!!.volumeControlStream
        mActivity!!.volumeControlStream = AudioManager.STREAM_MUSIC

        audioSwitch!!.start { _: List<AudioDevice?>?, audioDevice: AudioDevice? ->
            updateAudioDeviceIcon(
                audioDevice!!
            )
            Unit
        }
        SoundPoolManager.getInstance(ActivityMgr.getContext())
    }

    /**
     * 设置回调
     */
    fun setCallBacks(callBack: CallNumberBack.Builder.() -> Unit) = apply {
        callBacks = CallNumberBack().apply { setCallBack(callBack) }
    }

    /**
     * 是否注册成功
     */
    fun checkInitSuccess(): Boolean = checkTwilioInit

    /**
     * 是否激活call
     */
    fun checkCallActivate(): Boolean = checkCallActivate

    /**
     * 拨号
     */
    fun call(hashMap: HashMap<String, String>) {
        if (checkTwilioInit) {
            val connectOptions = ConnectOptions.Builder(AccountBean.twilioToken)
                .params(hashMap)
                .build()
            activeCall = mActivity?.let { Voice.connect(it, connectOptions, callListener()) }
        } else {
            ActivityMgr.finishFairyTableActivity()
            "未初始化成功,拒绝拨号操作".print()
        }
    }

    /**
     * 传递消息
     */
    fun handleIncomingCallIntent(intent: Intent) {
        if (intent.action != null) {
            val action = intent.action
            activeCallInvite = intent.getParcelableExtra(CallParameter.INCOMING_CALL_INVITE)
            activeCallNotificationId =
                intent.getIntExtra(CallParameter.INCOMING_CALL_NOTIFICATION_ID, 0)
            when (action) {
                CallParameter.ACTION_INCOMING_CALL -> { //可以打电话
                    startServer()
                }
                CallParameter.ACTION_INCOMING_CALL_NOTIFICATION -> { //来电通知
                }
                CallParameter.ACTION_CANCEL_CALL -> handleCancel()
                CallParameter.ACTION_FCM_TOKEN -> registerForCallInvites()
                CallParameter.ACTION_ACCEPT -> answer()
                else -> {
                    //nothing
                }
            }
        }
    }

    /**
     * 注册
     */
    fun registerForCallInvites() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener { task ->
            if (!task.isSuccessful) {
                "Registering error".printError()
                return@addOnCompleteListener
            }
            val fcmToken = task.result
            "Registering with FCM token = $fcmToken".printError()
            if (fcmToken.isNotNullEmpty()) {
                Voice.register(
                    AccountBean.twilioToken,
                    Voice.RegistrationChannel.FCM,
                    fcmToken!!,
                    registrationListener
                )
            }
        }
    }

    /**
     * activity 销毁需要调用
     */
    fun activityDes(activity: FragmentActivity) {
        audioSwitch!!.stop()
        activity.volumeControlStream = savedVolumeControlStream
    }


    /**
     * 动态注册本地广播
     */
    fun registerReceiver() {
        if (!isReceiverRegistered && voiceBroadcastReceiver != null && mActivity != null) {
            "注册广播".print()
            val intentFilter = IntentFilter()
            intentFilter.addAction(CallParameter.ACTION_INCOMING_CALL)
            intentFilter.addAction(CallParameter.ACTION_CANCEL_CALL)
            intentFilter.addAction(CallParameter.ACTION_FCM_TOKEN)
            LocalBroadcastManager.getInstance(mActivity!!).registerReceiver(
                voiceBroadcastReceiver!!, intentFilter
            )
            isReceiverRegistered = true
        }
    }

    /**
     * 解除注册
     */
    fun unregisterReceiver() {
        if (isReceiverRegistered && voiceBroadcastReceiver != null && mActivity != null) {
            "解除广播".print()
            LocalBroadcastManager.getInstance(mActivity!!)
                .unregisterReceiver(voiceBroadcastReceiver!!)
            isReceiverRegistered = false
        }
    }

    /**
     * 广播实现类
     */
    class VoiceBroadcastReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (action != null && (action == CallParameter.ACTION_INCOMING_CALL || action == CallParameter.ACTION_CANCEL_CALL)) {
                "广播接受到消息${intent.toJson()}".print()
                handleIncomingCallIntent(intent)
            }
        }
    }

    /**
     * 接听 电话
     */
    private fun answer() {
        if (mActivity != null) {
            callBacks?.build?.callSetUI?.invoke(true)
            SoundPoolManager.getInstance(mActivity!!)?.stopRinging()
            activeCallInvite!!.accept(mActivity!!, callListener())
            notificationManager!!.cancel(activeCallNotificationId)
            //if (alertDialog != null && alertDialog.isShowing()) {
            //    alertDialog.dismiss()
            //}
        }
    }

    /**
     * 取消
     */
    private fun handleCancel() {
        //if (alertDialog != null && alertDialog.isShowing()) {
        //    SoundPoolManager.getInstance(this).stopRinging()
        //    alertDialog.cancel()
        //}
    }


    /**
     * 注册监听
     */
    private fun registrationListener(): RegistrationListener {
        return object : RegistrationListener {
            override fun onRegistered(accessToken: String, fcmToken: String) {
                "Successfully registered FCM $fcmToken".printError()
                checkTwilioInit = true
                LiveEventBus.get<String>(LiveEventData.INIT_STATE).post("reg success")
            }

            override fun onError(
                error: RegistrationException,
                accessToken: String,
                fcmToken: String
            ) {
                checkTwilioInit = false
                if (error.errorCode == 20104 || error.errorCode == 31400){
                    LiveEventBus.get<String>(LiveEventData.INIT_STATE).post("20104")
                }else{
                    LiveEventBus.get<String>(LiveEventData.INIT_STATE).post("reg error")
                }
                callBacks?.build?.callError?.invoke(
                    CallErrorBean(
                        errorTitle = "Registration Error",
                        errorCode = error.errorCode,
                        errorMessage = error.message.toString()
                    )
                )
            }
        }
    }

    /**
     * 拨打电话监听
     */
    private fun callListener(): Call.Listener {
        return object : Call.Listener {
            override fun onRinging(call: Call) {
                SoundPoolManager.getInstance(ActivityMgr.getContext())?.playRinging()
                "Ringing".printError()
            }

            override fun onConnectFailure(call: Call, error: CallException) {
                audioSwitch?.deactivate()
                SoundPoolManager.getInstance(ActivityMgr.getContext())?.stopRinging()
                "Connect failure".printError()
                callBacks?.build?.callError?.invoke(
                    CallErrorBean(
                        errorCode = error.errorCode,
                        errorMessage = error.message.toString()
                    )
                )
                callBacks?.build?.callRefresh?.invoke(true)
            }

            override fun onConnected(call: Call) {
                audioSwitch?.activate()
                checkCallActivate = true
                callBacks?.build?.callAnswer?.invoke(true)
                SoundPoolManager.getInstance(ActivityMgr.getContext())?.stopRinging()
                "Connected".printError()
                activeCall = call
            }

            override fun onReconnecting(call: Call, callException: CallException) {
                "onReconnecting".printError()
            }

            override fun onReconnected(call: Call) {
                "onReconnected".printError()
            }

            override fun onDisconnected(call: Call, error: CallException?) {
                audioSwitch?.deactivate()
                checkCallActivate = false
                SoundPoolManager.getInstance(ActivityMgr.getContext())?.stopRinging()
                "Disconnected".printError()
                if (error != null) {
                    val message = String.format(
                        Locale.US,
                        "Call Error: %d, %s",
                        error.errorCode,
                        error.message
                    )
                    message.printError()
                    callBacks?.build?.callDisconnected?.invoke(
                        CallErrorBean(
                            errorCode = error.errorCode,
                            errorMessage = error.message.toString()
                        )
                    )
                }else{
                    callBacks?.build?.callDisconnected?.invoke(null)
                }
                //resetUI()
                callBacks?.build?.callRefresh?.invoke(true)
            }

            override fun onCallQualityWarningsChanged(
                call: Call,
                currentWarnings: MutableSet<Call.CallQualityWarning>,
                previousWarnings: MutableSet<Call.CallQualityWarning>
            ) {
                if (previousWarnings.size > 1) {
                    val intersection: MutableSet<Call.CallQualityWarning> = HashSet(currentWarnings)
                    currentWarnings.removeAll(previousWarnings)
                    intersection.retainAll(previousWarnings)
                    previousWarnings.removeAll(intersection)
                }
                //Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_LONG).show()
                callBacks?.build?.callQualityWarningsChanged?.invoke(CallErrorBean(errorMessage = "Newly raised warnings: $currentWarnings Clear warnings $previousWarnings"))
            }
        }
    }

    /**
     * 绑定通知
     */
    private fun startServer() {
        val acceptIntent = Intent(
            mActivity,
            IncomingCallNotificationService::class.java
        )
        acceptIntent.action = CallParameter.ACTION_ACCEPT
        acceptIntent.putExtra(CallParameter.INCOMING_CALL_INVITE, activeCallInvite)
        acceptIntent.putExtra(CallParameter.INCOMING_CALL_NOTIFICATION_ID, activeCallNotificationId)
        "Clicked accept startService".print()
        mActivity?.startService(acceptIntent)
    }

    /**
     * 解除绑定通知
     */
    private fun cancelServer() {
        SoundPoolManager.getInstance(ActivityMgr.getContext())?.stopRinging()
        if (activeCallInvite != null) {
            val intent = Intent(mActivity, IncomingCallNotificationService::class.java)
            intent.action = CallParameter.ACTION_REJECT
            intent.putExtra(CallParameter.INCOMING_CALL_INVITE, activeCallInvite)
            mActivity?.startService(intent)
        }
    }

    /*
     * Disconnect from Call
     */
    fun disconnect(): Boolean {
        "断开连接 object=${activeCall != null}".print()
        if (activeCall != null) {
            SoundPoolManager.getInstance(ActivityMgr.getContext())!!.playDisconnect()
            activeCall!!.disconnect()
            activeCall = null
            return true
        }
        return false
    }

    fun hold(): Boolean {
        if (activeCall != null) {
            val hold = !activeCall!!.isOnHold
            activeCall!!.hold(hold)
            //applyFabState(holdActionFab, hold)
            return true
        }
        return false
    }

    /**
     * 是否禁用话筒
     */
    fun mute(): Boolean {
        if (activeCall != null) {
            val mute = !activeCall!!.isMuted
            "mute= $mute".print()
            activeCall!!.mute(mute)
            //applyFabState(muteActionFab, mute)
            return activeCall!!.isMuted
        }
        return false
    }

    /**
     * 是否使用扬声器
     */
    fun openSpeak(isSpeak: Boolean): Boolean =
        SoundPoolManager.getInstance(ActivityMgr.getContext())?.setOpenSpeaker(isSpeak) ?: false


    /**
     * Show the current available audio devices.
     */
    private fun showAudioDevices() {
        val selectedDevice = audioSwitch!!.selectedAudioDevice
        val availableAudioDevices = audioSwitch!!.availableAudioDevices
        if (selectedDevice != null) {
            val selectedDeviceIndex = availableAudioDevices.indexOf(selectedDevice)
            val audioDeviceNames = ArrayList<String>()
            for (a in availableAudioDevices) {
                audioDeviceNames.add(a.name)
            }
            AlertDialog.Builder(mActivity)
                .setTitle(com.ultralab.ultracall.R.string.select_device)
                .setSingleChoiceItems(
                    audioDeviceNames.toTypedArray<CharSequence>(),
                    selectedDeviceIndex,
                    DialogInterface.OnClickListener { dialog: DialogInterface, index: Int ->
                        dialog.dismiss()
                        val selectedAudioDevice =
                            availableAudioDevices[index]
                        updateAudioDeviceIcon(selectedAudioDevice)
                        audioSwitch!!.selectDevice(selectedAudioDevice)
                    }).create().show()
        }
    }

    /**
     * Update the menu icon based on the currently selected audio device.
     */
    private fun updateAudioDeviceIcon(selectedAudioDevice: AudioDevice) {
        val audioDeviceMenuIcon: Int = when (selectedAudioDevice) {
            is AudioDevice.BluetoothHeadset -> {
                com.ultralab.ultracall.R.drawable.ic_bluetooth_white_24dp
            }
            is AudioDevice.WiredHeadset -> {
                com.ultralab.ultracall.R.drawable.ic_headset_mic_white_24dp
            }
            is AudioDevice.Earpiece -> {
                com.ultralab.ultracall.R.drawable.ic_phonelink_ring_white_24dp
            }
            is AudioDevice.Speakerphone -> {
                com.ultralab.ultracall.R.drawable.ic_volume_up_white_24dp
            }
        }
        "当前是 $selectedAudioDevice".printError()
    }
}