package com.ultralab.ultracall.model

import com.ultralab.ultracall.NetWorkUrl
import retrofit2.http.GET

/**
 * @ClassName: CallService
 * @Description: 接口
 * @Author: jason@leanktech.com
 * @Date: 2021/9/2 18:39
 */
interface HomeService {

    /**
     * 获取twilio token
     */
    @GET(NetWorkUrl.BASE_URL + NetWorkUrl.ACCESS_TOKEN)
    suspend fun getTwilioToken(): Any?
}