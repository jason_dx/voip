package com.ultralab.ultracall

import com.ultralab.base_lib.tool.keyvalue.KeyValueMgr
import com.ultralab.base_lib.tool.keyvalue.MmkvKeyValueMgr
import com.ultralab.base_lib.tool.network.RetrofitClient
import com.ultralab.ultracall.activity.country.SelectCountryViewModel
import com.ultralab.ultracall.activity.home.viewmodel.CallViewModel
import com.ultralab.ultracall.activity.home.viewmodel.ConfirmCallViewModel
import com.ultralab.ultracall.activity.home.viewmodel.FairyTalePageViewModel
import com.ultralab.ultracall.activity.home.viewmodel.HomeViewModel
import com.ultralab.ultracall.activity.user.viewmodel.LoginViewModel
import com.ultralab.ultracall.activity.user.viewmodel.MineViewModel
import com.ultralab.ultracall.activity.user.viewmodel.SettingViewModel
import com.ultralab.ultracall.activity.user.viewmodel.ShowFairyTaleViewModel
import com.ultralab.ultracall.model.CallRepository
import com.ultralab.ultracall.model.CallService
import com.ultralab.ultracall.model.HomeRepository
import com.ultralab.ultracall.model.HomeService
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import retrofit2.Retrofit

/**
 * Koin配置文件
 * @Author: jason@leanktech.com
 * @Date: 2021/8/20 15:51
 */

/**
 * viewModel模块
 */
val viewModelModule = module() {
    viewModel { HomeViewModel(get()) }
    viewModel { FairyTalePageViewModel() }
    viewModel { CallViewModel() }
    viewModel { MineViewModel() }
    viewModel { LoginViewModel() }
    viewModel { ShowFairyTaleViewModel() }
    viewModel { ConfirmCallViewModel() }
    viewModel { SettingViewModel() }
    viewModel { SelectCountryViewModel() }
}

/**
 * 本地数据模块
 */
val localModule = module {
    single<KeyValueMgr> { MmkvKeyValueMgr }
}

/**
 * 远程数据模块
 */
val remoteModule = module {
    single { RetrofitClient.getOkHttpClient() }
    single { RetrofitClient.getRetrofit(get(), NetWorkUrl.BASE_URL) }

    single<CallService> { get<Retrofit>().create(CallService::class.java) }
    single<HomeService> { get<Retrofit>().create(HomeService::class.java) }
}

/**
 * 数据仓库模块
 */
val repoModule = module {
    single { CallRepository(get(),get()) }
    single { HomeRepository(get(), get()) }
}

/**
 * 当需要构建你的ViewModel对象的时候，就会在这个容器里进行检索
 */
val appModule = listOf(viewModelModule, localModule, repoModule, remoteModule)