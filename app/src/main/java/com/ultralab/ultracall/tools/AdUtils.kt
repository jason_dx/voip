package com.ultralab.ultracall.tools

import android.content.Context
import androidx.annotation.NonNull
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.ultralab.base_lib.tool.print
import com.ultralab.ultracall.GoogleAd

/**
 * @Description: ad 工具类
 * @Author: jason@leanktech.com
 * @Date: 2021/10/11 13:16
 */
object AdUtils {

    private var mInterstitialAd: InterstitialAd? = null

    fun initAd(context: Context) {
        "开始load ad ".print()
        loadSplashAd(context)
        loadAppOpenAd(context)
    }

    /**
     * 先初始化 splash ad 资源
     */
    private fun loadSplashAd(context: Context) {
        val adRequest = AdRequest.Builder().build()
        InterstitialAd.load(
            context, GoogleAd.GOOGLE_AD_SPLASH_ID, adRequest,
            object : InterstitialAdLoadCallback() {
                override fun onAdLoaded(@NonNull interstitialAd: InterstitialAd) {
                    "Splash onAdLoaded success".print()
                    mInterstitialAd = interstitialAd
                }

                override fun onAdFailedToLoad(@NonNull loadAdError: LoadAdError) {
                    "Splash onAdFailedToLoad error${loadAdError.message}".print()
                    mInterstitialAd = null
                }
            })
    }

    /**
     * 获取 splash ad 对象
     */
    fun getSplashInterstitialAd() = mInterstitialAd

    /**
     * 初始化开屏ad
     */
    private fun loadAppOpenAd(context: Context) {

    }

}