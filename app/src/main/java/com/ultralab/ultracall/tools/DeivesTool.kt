package com.ultralab.ultracall.tools

import android.content.Context
import com.ultralab.base_lib.tool.ActivityMgr
import com.ultralab.base_lib.tool.printError
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.RandomAccessFile
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern


/**
 * @ClassName: DeviceTool
 * @Description: 获取本机信息
 * @Author: jason@leanktech.com
 * @Date: 2021/9/3 16:19
 */

private var sID: String? = null
private const val INSTALLATION = "INSTALLATION"

/**
 * 获取设备唯一标识
 * @param context 上下文对象，默认为ActivityMgr.getContext()
 * @return IMEI，获取不到返回空字符串
 */
@Synchronized
fun equipmentId(context: Context = ActivityMgr.getContext()): String? {
    if (sID == null) {
        val installation = File(context.filesDir, INSTALLATION)
        sID = try {
            if (!installation.exists()) writeInstallationFile(installation)
            readInstallationFile(installation)
        } catch (e: java.lang.Exception) {
            throw RuntimeException(e)
        }
    }
    "sid = ${getStringToNumber(sID ?: "")}".printError("SID")
    return sID?.let { getStringToNumber(it) }
}

@Throws(IOException::class)
private fun readInstallationFile(installation: File): String? {
    val f = RandomAccessFile(installation, "r")
    val bytes = ByteArray((f.length() as Long).toInt())
    f.readFully(bytes)
    f.close()
    return String(bytes)
}

@Throws(IOException::class)
private fun writeInstallationFile(installation: File) {
    val out = FileOutputStream(installation)
    val id = UUID.randomUUID().toString()
    out.write(id.toByteArray())
    out.close()
}

/**
 * 字符串中获取数字
 */
private fun getStringToNumber(string: String): String {
    val regEx = "[^0-9]"
    val p: Pattern = Pattern.compile(regEx)
    val m: Matcher = p.matcher(string)
    val strNumber = m.replaceAll("").trim()
    return if (strNumber.length > 8) {
        strNumber.substring(0, 8)
    } else {
        strNumber
    }
}
