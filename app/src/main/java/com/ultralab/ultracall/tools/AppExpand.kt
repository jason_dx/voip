package com.ultralab.ultracall.tools

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import com.alibaba.fastjson.JSONObject
import com.ultralab.base_lib.tool.ActivityMgr
import com.ultralab.base_lib.tool.isNotNullEmpty
import com.ultralab.base_lib.tool.toJson
import com.ultralab.base_lib.tool.toTime
import com.ultralab.ultracall.ActivityCallParameter
import com.ultralab.ultracall.R
import com.ultralab.ultracall.activity.home.activity.ShowFairyTaleActivity
import com.ultralab.ultracall.conversation.CallUtils
import com.ultralab.ultracall.local.AccountBean
import java.util.*


/**
 * @ClassName: AppExpand
 * @Description: 业务相关扩展功能
 * @Author: jason@leanktech.com
 * @Date: 2021/9/6 16:32
 */

/**
 * 去显示童话页面
 * @param areaCode 区号
 * @param phoneNumber 电话号码
 * @param saveDb 是否存
 */
fun goToCall(
    context: Context,
    phoneNumber: String,
    areaCode: String = "1",
    areaName: String = "",
    areaImage: String = "",
    saveDb: Boolean = true
) {
    if (phoneNumber.isNotNullEmpty()) {
        //直接拨打电话
        if (CallUtils.checkInitSuccess()) {
            context.startActivity(Intent(context, ShowFairyTaleActivity::class.java).apply {
                putExtra(ActivityCallParameter.CALL_AREA_CODE, areaCode)
                putExtra(ActivityCallParameter.CALL_PHONE_NUMBER, phoneNumber)
                putExtra(ActivityCallParameter.CALL_AREA_NAME, areaName)
                putExtra(ActivityCallParameter.CALL_AREA_IMAGE, areaImage)
                putExtra(ActivityCallParameter.CALL_IS_SAVE_DB, saveDb)
            })
        }
    }
}

/**
 * 辅助db包裹实际数据
 */
fun dbCreateTable(): HashMap<String, Any> {
    return hashMapOf(
        "userId" to AccountBean.userId.toLong(),
        "createTime" to System.currentTimeMillis()
    )
}

/**
 * 包装获取数据
 */
fun <T> Map<String, Any>?.getDbJsonData(key: String, mClass: Class<T>): MutableList<T> {
    if (this == null) {
        return mutableListOf()
    } else {
        val newList = mutableListOf<T>()
        val map = this[key] to hashMapOf<String, Any>()
        val dataList = map.toList()
        dataList.forEach { any ->
            if (any is ArrayList<*>) {
                any.forEach { item ->
                    val bean = JSONObject.parseObject(
                        item.toJson(),
                        mClass
                    )
                    newList.add(bean)
                }
            }
        }
        return newList
    }
}

/**
 * 判断今天是否需要弹出 退出阅读器推荐列表
 * @return false 弹出 true 不弹出
 * */
fun checkTodayCheckInBoolean(): Boolean {
    return if (AccountBean.todayCheckInString != 0L) {
        AccountBean.todayCheckInString.toTime("yyyy-MM-dd") == System.currentTimeMillis()
            .toTime("yyyy-MM-dd")
    } else {
        false
    }
}

/**
 * 获取项目版本号
 */
fun getVersionName(context: Context = ActivityMgr.getContext()): String? {
    val manager = context.packageManager
    var name: String? = null
    try {
        val info = manager.getPackageInfo(context.packageName, 0)
        name = info.versionName
    } catch (e: PackageManager.NameNotFoundException) {
        e.printStackTrace()
    }
    return name
}

/**
 * 获取当前时间星期+日期组合
 */
fun Long.timeStampToData(): String {
    val context = ActivityMgr.getContext()

    val c = Calendar.getInstance()
    c.timeZone = TimeZone.getTimeZone("GMT+8:00")
    //val mYear = java.lang.String.valueOf(c[Calendar.YEAR]) // 获取当前年份
    //val mMonth = java.lang.String.valueOf(c[Calendar.MONTH] + 1) // 获取当前月份
    //val mDay = java.lang.String.valueOf(c[Calendar.DAY_OF_MONTH]) // 获取当前月份的日期号码
    var mWay = java.lang.String.valueOf(c[Calendar.DAY_OF_WEEK])
    when (mWay) {
        "1" -> {
            mWay = context.resources.getString(R.string.sunday)
        }
        "2" -> {
            mWay = context.resources.getString(R.string.monday)
        }
        "3" -> {
            mWay = context.resources.getString(R.string.tuesday)
        }
        "4" -> {
            mWay = context.resources.getString(R.string.wednesday)
        }
        "5" -> {
            mWay = context.resources.getString(R.string.thursday)
        }
        "6" -> {
            mWay = context.resources.getString(R.string.friday)
        }
        "7" -> {
            mWay = context.resources.getString(R.string.saturday)
        }
    }
    return "$mWay ${this.toTime("yyyy-MM-dd")}"
}

/**
 * 显示通用dialog 弹窗
 */
fun showMessageDialog(context: Context,message: String) {
    //显示签到成功弹窗
    val builder = AlertDialog.Builder(context)
    builder.setMessage(message)
    builder.setPositiveButton("OK") { dialog, _ ->
        dialog.dismiss()
    }
    builder.create().apply {
        setCancelable(false)
        setCanceledOnTouchOutside(false)
    }.show()
}

/**
 * 获取对应城市广告累积消耗
 */
fun String.getAreaCreditsNumber(): String {
    val hashMap = hashMapOf<String, String>(
        "Afghanistan" to "14000",
        "Albania" to "10600",
        "Algeria" to "3754",
        "Andorra" to "1213",
        "Angola" to "2640",
        "Antarctica" to "10900",
        "Argentina" to "1064",
        "Armenia" to "12700",
        "Aruba" to "5651",
        "Australia" to "693",
        "Austria" to "8455",
        "Azerbaijan" to "13300",
        "Bahrain" to "4183",
        "Bangladesh" to "2082",
        "Belarus" to "26000",
        "Belgium" to "5033",
        "Belize" to "6600",
        "Benin" to "17080",
        "Bhutan" to "4637",
        "Bolivia" to "11000",
        "Bosnia and Herzegovina" to "9488",
        "Botswana" to "8250",
        "Brazil" to "338",
        "Brunei" to "2269",
        "Bulgaria" to "5750",
        "Burkina Faso" to "9075",
        "Burundi" to "31350",
        "Cambodia" to "3300",
        "Cameroon" to "6592",
        "Canada" to "1200",
        "Cape Verde" to "10800",
        "Central African Republic" to "8085",
        "Chad" to "19000",
        "Chile" to "421",
        "China" to "1422",
        "Christmas Island" to "693",
        "Cocos Islands" to "19800",
        "Colombia" to "693",
        "Comoros" to "1130",
        "Congo" to "9700",
        "Cook Islands" to "11500",
        "Costa Rica" to "42578",
        "Croatia" to "1345",
        "Cuba" to "825",
        "Cyprus" to "44140",
        "Czech Republic" to "12460",
        "Coate d'Ivoire" to "1990",
        "Denmark" to "734",
        "Djibouti" to "20800",
        "Ecuador" to "8374",
        "Egypt" to "5014",
        "El Salvador" to "7376",
        "Equatorial Guinea" to "16060",
        "Eritrea" to "14685",
        "Estonia" to "1106",
        "Ethiopia" to "16273",
        " Falkland Islands" to "55275",
        "Faroe Islands" to "15980",
        "Fiji" to "16995",
        "Finland" to "4125",
        "France" to "908",
        "French Polynesia" to "14677",
        "Gabon" to "27019",
        "Gambia" to "21700",
        "Georgia" to "10634",
        "Germany" to "198",
        "Ghana" to "17845",
        "Gibraltar" to "4818",
        "Greece" to "1015",
        "Greenland" to "32522",
        "Guatemala" to "1964",
        "Guinea" to "8333",
        "Guinea-Bissau" to "18728",
        "Guyana" to "30000",
        "Grenada" to "15139",
        "Haiti" to "14025",
        "Honduras" to "7838",
        "Hong Kong" to "1262",
        "Hungary" to "4414",
        "India" to "998",
        "Indonesia" to "2503",
        "Iran" to "8415",
        "Iraq" to "9570",
        "Ireland" to "391",
        "Isle Of Man" to "314",
        "Israel" to "2145",
        "Italy" to "248",
        "Japan" to "1221",
        "Jordan" to "9034",
        "Kazakhstan" to "1543",
        "Kenya" to "12700",
        "Kiribati" to "19157",
        "Kuwait" to "3096",
        "Kyrgyzstan" to "8663",
        "Laos" to "5033",
        "Latvia" to "22900",
        "Lebanon" to "5693",
        "Lesotho" to "14000",
        "Liberia" to "25163",
        "Libya" to "15593",
        "Liechtenstein" to "5033",
        "Lithuania" to "10238",
        "Luxembourg" to "12090",
        "Myanmar" to "6501",
        "Macao" to "8943",
        "Macedonia" to "35327",
        "Madagascar" to "19355",
        "Malawi" to "1251",
        "Malaysia" to "47817",
        "Maldives" to "9108",
        "Mali" to "10478",
        "Malta" to "16352",
        "Marshall Islands" to "22275",
        "Mauritania" to "6897",
        "Mauritius" to "44344",
        "Mayotte" to "299",
        "Mexico" to "10436",
        "Micronesia" to "23843",
        "Moldova" to "4769",
        "Monaco" to "850",
        "Mongolia" to "10156",
        "Montenegro" to "977",
        "Morocco" to "13766",
        "Mozambique" to "9240",
        "Namibia" to "8115",
        "Nauru" to "51150",
        "Nepal" to "10148",
        "Netherlands" to "15040",
        "Netherlands Antilles" to "8283",
        "New Caledonia" to "11666",
        "New Zealand" to "2475",
        "Nicaragua" to "4216",
        "Niger" to "12581",
        "Nigeria" to "7095",
        "Niue" to "52800",
        "North Korea" to "25773",
        "Norway" to "206",
        "Oman" to "6724",
        "Pakistan" to "3337",
        "Palau" to "16467",
        "Panama" to "1064",
        "Papua New Guinea" to "27671",
        "Paraguay" to "1320",
        "Peru" to "340",
        "Philippines" to "6848",
        "Pitcairn" to "264000",
        "Poland" to "9694",
        "Portugal" to "388",
        "Puerto Rico" to "1200",
        "Qatar" to "11303",
        "Russia" to "183",
        "Romania" to "1543",
        "Rwanda" to "18728",
        "Saint Barthelemy" to "1502",
        "Samoa" to "45334",
        "San Marino" to "11500",
        "Sao Tome And Principe" to "31754",
        "Saudi Arabia" to "30377",
        "Senegal" to "28875",
        "Serbia" to "5590",
        "Seychelles" to "15600",
        "Sierra Leone" to "9628",
        "Singapore" to "29700",
        "Slovakia" to "24750",
        "Slovenia" to "842",
        "Solomon Islands" to "4307",
        "Somalia" to "11220",
        "South Africa" to "59474",
        "South Korea" to "21368",
        "Spain" to "3878",
        "Sri Lanka" to "595",
        "Saint Helena" to "190",
        "Saint Pierre And Miquelon" to "10919",
        "Sudan" to "9305",
        "Suriname" to "8168",
        "Swaziland" to "12623",
        "Sweden" to "140",
        "Switzerland" to "1031",
        "Syria" to "8073",
        "The Democratic Republic Of Congo" to "1345",
        "Timor-Leste" to "7673",
        "Taiwan" to "16500",
        "Tajikistan" to "1980",
        "Tanzania" to "8613",
        "Thailand" to "15100",
        "Togo" to "18472",
        "Tokelau" to "40582",
        "Tonga" to "39400",
        "Tunisia" to "1964",
        "Turkey" to "51975",
        "Turkmenistan" to "1827",
        "Tuvalu" to "6435",
        "Trinidad and Tobago" to "39039",
        "United States" to "600",
        "United Arab Emirates" to "9323",
        "Uganda" to "10494",
        "United Kingdom" to "314",
        "Ukraine" to "1200",
        "Uruguay" to "2360",
        "Uzbekistan" to "6925",
        "Vanuatu" to "36506",
        "Vatican" to "248",
        "Venezuela" to "767",
        "Vietnam" to "3671",
        "Wallis And Futuna" to "22952",
        "Yemen" to "9220",
        "Zambia" to "14561",
        "Zimbabwe" to "4232",
    )
    return if (hashMap[this].isNotNullEmpty()) {
        hashMap[this].toString()
    } else {
        ""
    }
}
