package com.ultralab.ultracall.tools.empty

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.ultralab.ultracall.R


/**
 *空数据视图
 */
class EmptyLayout @JvmOverloads constructor(
    private val mContext: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(mContext, attrs, defStyleAttr), View.OnClickListener {

    //------------------------------公共属性开始------------------------------
    var clickAction: (() -> Unit)? = null
    //------------------------------公共属性结束------------------------------


    private var mView: View? = null

    //内部组件
    private lateinit var emptyBgView: LinearLayout
    private lateinit var emptyContentView: LinearLayout
    private lateinit var emptyHintView: TextView
    private lateinit var emptyImgView: ImageView

    init {
        if (mView == null) {
                val mInflater = LayoutInflater.from(mContext)
                mView = mInflater.inflate(R.layout.widget_empty, null)

                emptyBgView = mView!!.findViewById(R.id.empty_bg)
                emptyContentView = mView!!.findViewById(R.id.empty_content)
                emptyImgView = mView!!.findViewById(R.id.empty_img)
                emptyHintView = mView!!.findViewById(R.id.empty_text)
            }

            emptyContentView.setOnClickListener(this)

        if (attrs != null) {
            val a = mContext.obtainStyledAttributes(attrs, R.styleable.EmptyLayout, defStyleAttr, 0)

            val emptyHintTxt = a.getText(R.styleable.EmptyLayout_emptyHint)
            if (emptyHintTxt.isNullOrEmpty()){
                emptyHintView.visibility=View.GONE
            }else{
                emptyHintView.visibility=View.VISIBLE
                emptyHintView.text = a.getText(R.styleable.EmptyLayout_emptyHint)
            }
            val emptyHintColor = a.getColor(R.styleable.EmptyLayout_emptyHintColor,resources.getColor(R.color.empty_hint_color))
            emptyHintView.setTextColor(emptyHintColor)

            val emptyImgIcon = a.getDrawable(R.styleable.EmptyLayout_emptyImg)
            if (emptyImgIcon == null) {
                //emptyImgView.setImageDrawable(resources.getDrawable(R.drawable.default_empty_detail))
            }else{
                emptyImgView.setImageDrawable(emptyImgIcon)
            }

            val emptyBgColor = a.getColor(R.styleable.EmptyLayout_emptyBgColor,Color.WHITE)
            emptyBgView.setBackgroundColor(emptyBgColor)

            val contentMarginTop = a.getDimension(R.styleable.EmptyLayout_emptyContentMarginTop,-1f)

            val contentPadding = a.getDimension(R.styleable.EmptyLayout_emptyContentPadding,resources.getDimension(R.dimen.empty_padding))
            emptyBgView.setPadding(contentPadding.toInt(),0,contentPadding.toInt(),0)

            val contentViewLayoutParams= emptyContentView.layoutParams as LinearLayout.LayoutParams
            if (contentMarginTop>=0){
                contentViewLayoutParams.setMargins(0,contentMarginTop.toInt(),0,0)
                contentViewLayoutParams.gravity=Gravity.CENTER_HORIZONTAL
            }else{
                contentViewLayoutParams.gravity=Gravity.CENTER
            }
        }
        addView(
            mView, LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
        )
        orientation = VERTICAL
    }


    override fun onClick(v: View?) {
        if (clickAction!=null&&v!=null&&v.id==R.id.empty_content){
            clickAction?.invoke()
        }
    }
}

