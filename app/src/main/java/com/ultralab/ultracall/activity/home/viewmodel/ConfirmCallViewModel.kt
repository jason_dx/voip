package com.ultralab.ultracall.activity.home.viewmodel

import androidx.lifecycle.MutableLiveData
import com.ultralab.base_lib.base.BaseViewMode
import com.ultralab.base_lib.tool.init

/**
 * @ClassName: ConfirmCallViewModel
 * @Description: 确认通话页面 数据类
 * @Author: jason@leanktech.com
 * @Date: 2021/9/6 14:39
 */
class ConfirmCallViewModel : BaseViewMode() {
    /**
     * 区号
     */
    var areaCode = MutableLiveData<String>().init("")

    /**
     * 手机号
     */
    var phoneNumber = MutableLiveData<String>().init("")

    /**
     * 地区名称
     */
    var areaName = MutableLiveData<String>().init("")

    /**
     * 地区国旗
     */
    var areaImage = MutableLiveData<String>().init("")

    /**
     * 当前通话剩余时间
     */
    val remainTime = MutableLiveData<String>().init("")

    /**
     * 单词消耗多少值
     */
    val singleConsumption = MutableLiveData<Int>().init(0)

    /**
     * 显示当前时间
     */
    val nowTimeStr = MutableLiveData<String>().init("")

    /**
     * 显示当前日期
     */
    val currentDateStr = MutableLiveData<String>().init("")


    fun getNetWordTime() {
//        val connection: HttpUrlConnection =
//            URL("http://www.baidu.com").openConnection() as HttpURLConnection connection . connect ()
//        val date: Long = connection.getDate()
//        val format: SimpleDateFormat = SimpleDateFormat(
//            "yyyy-MM-dd",
//            Locale.CHINA
//        )
//        format . setTimeZone
//        TimeZone.getTimeZone("GMT+08")
//        val dateString: String = format.format(Date(date))
    }
}