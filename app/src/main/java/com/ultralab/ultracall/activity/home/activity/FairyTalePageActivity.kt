package com.ultralab.ultracall.activity.home.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import com.gyf.immersionbar.ImmersionBar
import com.ultralab.base_lib.activity.BaseActivity
import com.ultralab.base_lib.tool.showToast
import com.ultralab.ultracall.ActivityCallParameter
import com.ultralab.ultracall.R
import com.ultralab.ultracall.activity.country.SelectCountryActivity
import com.ultralab.ultracall.activity.home.dialog.ConfirmCallDialog
import com.ultralab.ultracall.activity.home.viewmodel.FairyTalePageViewModel
import com.ultralab.ultracall.databinding.FairyTalePageActivityBinding
import com.ultralab.ultracall.local.AccountBean
import com.ultralab.ultracall.tools.showMessageDialog
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * @ClassName: FairyTalePageDialog
 * @Description: 通话页面
 * @Author: jason@leanktech.com
 * @Date: 2021/8/24 17:01
 */
class FairyTalePageActivity : BaseActivity<FairyTalePageActivityBinding>() {

    private val viewModel by viewModel<FairyTalePageViewModel>()

    override fun getLayoutId(): Int = R.layout.fairy_tale_page_activity

    override val showToolBar: Boolean
        get() = false

    /** 字符串拼接 */
    private val strBuild by lazy {
        StringBuilder()
    }

    override fun finish() {
        super.finish()
        //overridePendingTransition(0, 0)

    }

    override fun initView() {
        //overridePendingTransition(0, 0)
        bindingView.vm = viewModel
        bindingView.lifecycleOwner = this
        bindingView.presenter = this
        bindingView.ac = AccountBean
        //设置状态栏颜色
        ImmersionBar.with(this).statusBarColor(R.color.green_shen).init()
        //禁止软键盘弹出
        window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        //
        bindingView.rootConstrLayout.setOnClickListener {
            finish()
        }
        //clear按钮长按全部删除
        bindingView.clearEditTextAreaView.setOnLongClickListener {
            bindingView.inputNumberEt.text =
                strBuild.delete(0, strBuild.toString().length)
            true
        }
    }

    override fun loadData(isRefresh: Boolean) {

    }

    override fun onClick(v: View?) {
        super.onClick(v)
        if (v != null) {
            when (v.id) {
                R.id.fab_call_action -> {
                    val textCode = bindingView.showFCodeTv.text.toString()
                    val phone = bindingView.inputNumberEt.text.toString()
                    val image = if (AccountBean.lastSelectedCountry.isNotEmpty()) {
                        val split = AccountBean.lastSelectedCountry.split(",")
                        split[0] ?: ""
                    } else {
                        ""
                    }
                    val textName = if (AccountBean.lastSelectedCountry.isNotEmpty()) {
                        val split = AccountBean.lastSelectedCountry.split(",")
                        split[1] ?: ""
                    } else {
                        ""
                    }
                    if (textCode.isNotEmpty() && textName.isNotEmpty() && phone.isNotEmpty()) {
                        val code = textCode.substring(1, textCode.length)
                        if (code == "86") {
                            showMessageDialog(this,resources.getString(R.string.not_open))
                        } else {
                            //goto next
                            showCallInfo(phone, code, textName, image)
                        }
                    } else {
                        "Number is too short".showToast()
                    }
                }
                R.id.show_area_info_view -> {
                    startActivity(Intent(this, SelectCountryActivity::class.java))
                }
                //删除edit 内容
                R.id.clear_edit_text_area_view -> {
                    if (strBuild.toString().isNotEmpty()) {
                        bindingView.inputNumberEt.text =
                            strBuild.deleteCharAt(strBuild.toString().length - 1)
                    }
                }
                //1
                R.id.input_num_1_ly -> {
                    bindingView.inputNumberEt.text = strBuild.append("1").toString()
                }
                R.id.input_num_2_ly -> {
                    bindingView.inputNumberEt.text = strBuild.append("2").toString()
                }
                R.id.input_num_3_ly -> {
                    bindingView.inputNumberEt.text = strBuild.append("3").toString()
                }
                R.id.input_num_4_ly -> {
                    bindingView.inputNumberEt.text = strBuild.append("4").toString()
                }
                R.id.input_num_5_ly -> {
                    bindingView.inputNumberEt.text = strBuild.append("5").toString()
                }
                R.id.input_num_6_ly -> {
                    bindingView.inputNumberEt.text = strBuild.append("6").toString()
                }
                R.id.input_num_7_ly -> {
                    bindingView.inputNumberEt.text = strBuild.append("7").toString()
                }
                R.id.input_num_8_ly -> {
                    bindingView.inputNumberEt.text = strBuild.append("8").toString()
                }
                R.id.input_num_9_ly -> {
                    bindingView.inputNumberEt.text = strBuild.append("9").toString()
                }
                R.id.input_num_0_ly -> {
                    bindingView.inputNumberEt.text = strBuild.append("0").toString()
                }
                R.id.input_num_star_ly -> {
                    bindingView.inputNumberEt.text = strBuild.append("*").toString()
                }
                R.id.input_num_well_ly -> {
                    bindingView.inputNumberEt.text = strBuild.append("#").toString()
                }
                else -> {
                }
            }
        }
    }

    /**
     * 调用call
     * @param phone 输入的手机号
     * @param code  区号
     * @param name 地区名称
     * @param image 地区国旗
     */
    private fun showCallInfo(
        phone: String = "",
        code: String = "",
        name: String,
        image: String
    ) {
        ConfirmCallDialog().apply {
            arguments = Bundle().apply {
                putString(ActivityCallParameter.CALL_AREA_CODE, code)
                putString(ActivityCallParameter.CALL_PHONE_NUMBER, phone)
                putString(ActivityCallParameter.CALL_AREA_NAME, name)
                putString(ActivityCallParameter.CALL_AREA_IMAGE, image)
                putBoolean(ActivityCallParameter.CALL_IS_SAVE_DB, true)
            }
            freeCallBack = {
                if (it) {
                    finish()
                }
            }
        }.show(supportFragmentManager, "ConfirmCallInfo")
    }
}