package com.ultralab.ultracall.activity.home.activity

import android.content.Intent
import androidx.annotation.NonNull
import com.google.android.gms.ads.AdError
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.FullScreenContentCallback
import com.google.android.gms.ads.LoadAdError
import com.ultralab.base_lib.activity.BaseActivity
import com.ultralab.ultracall.R
import com.ultralab.ultracall.databinding.InterstitialAdActivityBinding

import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.ultralab.base_lib.tool.print
import com.ultralab.ultracall.GoogleAd
import com.ultralab.ultracall.tools.AdUtils


/**
 * @ClassName: InterstitialAdActivity
 * @Description: 类作用描述
 * @Author: jason@leanktech.com
 * @Date: 2021/10/9 10:45
 */
class InterstitialAdActivity : BaseActivity<InterstitialAdActivityBinding>() {


    override fun getLayoutId(): Int = R.layout.interstitial_ad_activity

    override fun initView() {
        bindingView.lifecycleOwner = this

    }

    override fun loadData(isRefresh: Boolean) {
        //通过
        if (AdUtils.getSplashInterstitialAd() != null) {
            AdUtils.getSplashInterstitialAd()?.let {
                it.fullScreenContentCallback = object : FullScreenContentCallback() {
                    override fun onAdDismissedFullScreenContent() {
                        super.onAdDismissedFullScreenContent()
                        "onAdDismissedFullScreenContent".print()
                        startActivity(Intent(this@InterstitialAdActivity, HomeActivity::class.java))

                    }

                    override fun onAdFailedToShowFullScreenContent(p0: AdError) {
                        super.onAdFailedToShowFullScreenContent(p0)
                        "onAdFailedToShowFullScreenContent".print()
                    }

                    override fun onAdImpression() {
                        super.onAdImpression()
                        "onAdImpression".print()
                    }

                    override fun onAdShowedFullScreenContent() {
                        super.onAdShowedFullScreenContent()
                        "onAdShowedFullScreenContent".print()
                    }
                }
                it.show(this@InterstitialAdActivity)
            }
        }else{
            startActivity(Intent(this@InterstitialAdActivity, HomeActivity::class.java))
            finish()
        }
    }

    override fun onStop() {
        super.onStop()
        finish()
    }
}