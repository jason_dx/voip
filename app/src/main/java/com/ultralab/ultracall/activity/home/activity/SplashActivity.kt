package com.ultralab.ultracall.activity.home.activity

import android.Manifest
import android.content.Intent
import android.graphics.Paint
import android.view.View
import com.alibaba.android.arouter.facade.annotation.Route
import com.gyf.immersionbar.BarHide
import com.gyf.immersionbar.ImmersionBar
import com.hjq.permissions.OnPermissionCallback
import com.hjq.permissions.XXPermissions
import com.ultralab.base_lib.activity.BaseActivity
import com.ultralab.base_lib.tool.AppTool
import com.ultralab.base_lib.tool.showToast
import com.ultralab.ultracall.ARoutePath
import com.ultralab.ultracall.R
import com.ultralab.ultracall.UrlPath
import com.ultralab.ultracall.activity.H5Activity
import com.ultralab.ultracall.databinding.SplashActivityBinding
import com.ultralab.ultracall.local.AccountBean
import com.ultralab.ultracall.tools.AdUtils
import com.ultralab.ultracall.tools.equipmentId
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/**
 * @ClassName: SplashActivity
 * @Description: 开屏页面
 * @Author: jason@leanktech.com
 * @Date: 2021/8/23 15:51
 */
@Route(path = ARoutePath.ROUTE_SPLASH_PATH)
class SplashActivity : BaseActivity<SplashActivityBinding>() {

    override fun getLayoutId(): Int = R.layout.splash_activity

    override fun initView() {
        ImmersionBar.with(this).hideBar(BarHide.FLAG_HIDE_BAR).fullScreen(true).init()
        bindingView.lifecycleOwner = this
        bindingView.presenter = this
        bindingView.ab = AccountBean

        bindingView.privacyPolicyTv.paint.flags = Paint.UNDERLINE_TEXT_FLAG
        bindingView.userAgreementTv.paint.flags = Paint.UNDERLINE_TEXT_FLAG

        if (!AccountBean.firstGoApp && AccountBean.userId.isBlank()) {
            AccountBean.userId = equipmentId() ?: "00000001"
        }

        //第二次进入 直接走逻辑
        if (AccountBean.firstGoApp) {
            checkRecordAudio()
        } else {
            //starHomeAc()
        }
        //初始化ad
        AdUtils.initAd(this.applicationContext)
    }

    override fun loadData(isRefresh: Boolean) {

    }

    override fun onClick(v: View?) {
        super.onClick(v)
        if (v != null) {
            AppTool.singleClick(v) {
                when (v.id) {
                    R.id.privacy_policy_tv -> {
                        val intent = Intent(this, H5Activity::class.java).apply {
                            putExtra("h5url", UrlPath.PRIVACY_POLICY)
                        }
                        startActivity(intent)
                    }
                    R.id.user_agreement_tv -> {
                        val intent = Intent(this, H5Activity::class.java).apply {
                            putExtra("h5url", UrlPath.USER_AGREEMENT)
                        }
                        startActivity(intent)
                    }
                    R.id.start_btn -> {
                        //同意隐私政策与权限
                        if (bindingView.consentAgreementCb.isChecked) {
                            checkRecordAudio()
                            AccountBean.firstGoApp = true
                        } else {
                            resources.getString(R.string.please_agree_policy).showToast()
                        }
                    }
                }
            }
        }
    }

    /**
     * 跳转主页
     */
    private fun starHomeAc() {
        var checkForInitAdStates = true
        var checkCounter = 0
        //做操作
        CoroutineScope(Dispatchers.Main).launch {
            delay(1000)

            while (checkForInitAdStates) {
                //如果超过5s 正常网络出问题。直接进入主页
                if (checkCounter <= 5) {
                    if (AdUtils.getSplashInterstitialAd() != null) {
                        checkForInitAdStates = false
                        startActivity(
                            Intent(
                                this@SplashActivity,
                                InterstitialAdActivity::class.java
                            )
                        )
                        finish()
                    } else {
                        delay(1000)
                        checkCounter++
                    }
                } else {
                    checkForInitAdStates = false
                    startActivity(Intent(this@SplashActivity, HomeActivity::class.java))
                    finish()
                }
            }
        }
    }

    /**
     * 检查是否允许了录音权限
     */
    private fun checkRecordAudio() {
        if (XXPermissions.isGranted(
                this,
                Manifest.permission.RECORD_AUDIO
            )
        ) {
            starHomeAc()
        } else {
            XXPermissions.with(this)
                .permission(Manifest.permission.RECORD_AUDIO)
                .request(object : OnPermissionCallback {
                    override fun onGranted(
                        permissions: MutableList<String>?,
                        all: Boolean
                    ) {
                        if (all) {
                            starHomeAc()
                        }
                    }

                    override fun onDenied(
                        permissions: MutableList<String>?,
                        never: Boolean
                    ) {
                        starHomeAc()
                    }
                })
        }
    }
}