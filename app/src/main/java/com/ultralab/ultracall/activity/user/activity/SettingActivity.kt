package com.ultralab.ultracall.activity.user.activity

import android.content.Intent
import android.view.View
import com.gyf.immersionbar.ImmersionBar
import com.ultralab.base_lib.activity.BaseActivity
import com.ultralab.base_lib.tool.AppTool
import com.ultralab.ultracall.R
import com.ultralab.ultracall.UrlPath
import com.ultralab.ultracall.activity.H5Activity
import com.ultralab.ultracall.activity.user.viewmodel.SettingViewModel
import com.ultralab.ultracall.databinding.SettingActivityBinding
import com.ultralab.ultracall.tools.getVersionName
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * @ClassName: SettingActivity
 * @Description: 设置
 * @Author: jason@leanktech.com
 * @Date: 2021/9/8 16:10
 */
class SettingActivity : BaseActivity<SettingActivityBinding>() {

    private val mViewMode by viewModel<SettingViewModel>()

    override fun getLayoutId(): Int = R.layout.setting_activity

    override fun initView() {
        //设置状态栏高度
        toolBar?.setToolBarBg(resources.getColor(R.color.green_shen))
        setTitleContent(titleName = "Setting")
        toolBar?.setToolBarTitleBg(resources.getColor(R.color.white))
        ImmersionBar.with(this).statusBarColor(R.color.green_shen).init()

        bindingView.vm = mViewMode
        bindingView.lifecycleOwner = this
        bindingView.presenter = this

    }

    override fun loadData(isRefresh: Boolean) {
        mViewMode.versionInfo.value = "${resources.getString(R.string.app_name)} ${getVersionName()}"
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        if (v != null) {
            AppTool.singleClick(v) {
                when (v.id) {
                    R.id.about_us_view -> {
                        val intent = Intent(this, H5Activity::class.java).apply {
                            putExtra("h5url", UrlPath.ABOUT)
                        }
                        startActivity(intent)
                    }
                }
            }
        }
    }
}