package com.ultralab.ultracall.activity.home.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.jeremyliao.liveeventbus.LiveEventBus
import com.ultralab.base_lib.adapter.ItemClickPresenter
import com.ultralab.base_lib.adapter.MultiTypeAdapter
import com.ultralab.base_lib.fragment.BaseFragment
import com.ultralab.base_lib.tool.*
import com.ultralab.ultracall.ActivityCallParameter
import com.ultralab.ultracall.LiveEventData
import com.ultralab.ultracall.R
import com.ultralab.ultracall.activity.bean.DialListBean
import com.ultralab.ultracall.activity.bean.EmptyItemBean
import com.ultralab.ultracall.activity.home.activity.FairyTalePageActivity
import com.ultralab.ultracall.activity.home.dialog.ConfirmCallDialog
import com.ultralab.ultracall.activity.home.viewmodel.CallViewModel
import com.ultralab.ultracall.conversation.CallUtils
import com.ultralab.ultracall.databinding.CallFragmentBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * @ClassName: HomeFragment
 * @Description: 首页展示 call 功能
 * @Author: jason@leanktech.com
 * @Date: 2021/8/26 16:47
 */
class CallFragment : BaseFragment<CallFragmentBinding>(), ItemClickPresenter<Any> {

    private val mViewModel by viewModel<CallViewModel>()

    private val mAdapter by lazy {
        MultiTypeAdapter(
            mContext,
            mViewModel.dialList,
            object : MultiTypeAdapter.MultiViewTyper {
                override fun getViewType(item: Any): Int =
                    when (item) {
                        is EmptyItemBean -> ItemType.EMPTY
                        else -> ItemType.NORMAL
                    }
            }).apply {
            addViewTypeToLayoutMap(ItemType.EMPTY, R.layout.item_change_empty)
            addViewTypeToLayoutMap(ItemType.NORMAL, R.layout.dial_item)
            itemPresenter = this@CallFragment
        }

    }

    override fun getLayoutId(): Int = R.layout.call_fragment

    @SuppressLint("ResourceType")
    override fun initView() {
        bindingView.vm = mViewModel
        bindingView.lifecycleOwner = this
        bindingView.presenter = this
        //空布局高度
        mViewModel.emptyMaxHeight =
            (AppTool.showContentHeight - StatusBarTool.getStatusBarHeight() - 44F.dp2px() - 50F.dp2px())

        bindingView.callListRv.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = mAdapter
        }

        CallUtils.setCallBacks {
            callError {
                ActivityMgr.finishFairyTableActivity()
                it.toStringMessage().printError()
                if (it.errorCode == 31005) {
                    resources.getString(R.string.call_connection).showToast()
                }
            }
            callAnswer {
                //开始接听
                //计时开始
            }
            callDisconnected {
                //断开连接
                //当有计时开始,计时才能结束
                //todo 当twilio在onConnected状态可能会立马调用Disconnected
                ActivityMgr.finishFairyTableActivity()
            }
        }

        //监听注册状态
        LiveEventBus.get(LiveEventData.UPDATE_LIST, Boolean::class.java).observe(this, {
            if (it) {
                //更新数据
                mViewModel.loadData(true)
            }
        })

        mViewModel.showLoadingTime.observe(this, {
            if (!it){
                CoroutineScope(Dispatchers.Main).launch {
                    delay(500)
                    mViewModel.showLoading.value = false
                    bindingView.rotateView.stop()
                }
            }
        })
        //throw RuntimeException("Test Crash") // Force a crash
    }

    override fun loadData(isRefresh: Boolean) {
        mViewModel.loadData()
        bindingView.rotateView.start()
        mViewModel.showLoading.value = true
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        if (v != null) {
            AppTool.singleClick(v) {
                when (v.id) {
                    R.id.call_action_fab -> {
                        startActivity(Intent(requireContext(), FairyTalePageActivity::class.java))
                    }
                }
            }
        }
    }

    /**
     * 区号拼接 1+8004732255
     * CallUtils.call(hashMapOf("to" to "18004732255"))
     */
    override fun onItemClick(v: View?, item: Any) {
        if (v != null) {
            when (v.id) {
                R.id.click_phone_area_view -> {
                    if (item is DialListBean) {
                        ConfirmCallDialog().apply {
                            arguments = Bundle().apply {
                                putString(
                                    ActivityCallParameter.CALL_AREA_CODE,
                                    item.areaCode.toString()
                                )
                                putBoolean(ActivityCallParameter.CALL_IS_SAVE_DB, false)
                                putString(ActivityCallParameter.CALL_PHONE_NUMBER, item.phoneNumber)
                                putString(ActivityCallParameter.CALL_AREA_NAME, item.areaName)
                                putString(ActivityCallParameter.CALL_AREA_IMAGE, item.areaImage)
                            }
                        }.show(childFragmentManager, "ConfirmCallInfo")
                    }
                }
            }
        }
    }
}