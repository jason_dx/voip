package com.ultralab.ultracall.activity

import android.annotation.SuppressLint
import com.alibaba.android.arouter.facade.annotation.Autowired
import com.alibaba.android.arouter.facade.annotation.Route
import com.gyf.immersionbar.ImmersionBar
import com.ultralab.base_lib.activity.BaseActivity
import com.ultralab.ultracall.ARoutePath
import com.ultralab.ultracall.R
import com.ultralab.ultracall.databinding.H5ActivityBinding

/**
 * @ClassName: H5Activity
 * @Description: 加载h5页面
 * @Author: jason@leanktech.com
 * @Date: 2021/9/17 13:44
 */
@Route(path = ARoutePath.H5_PATH)
class H5Activity : BaseActivity<H5ActivityBinding>() {

    /**
     * H5页面url
     */
    @Autowired(name = "h5url")
    @JvmField
    var h5url: String = ""

    /**
     * H5页面标题
     */
    @Autowired(name = "h5title")
    @JvmField
    var h5title: String? = ""


    override fun getLayoutId(): Int = R.layout.h5_activity

    @SuppressLint("SetJavaScriptEnabled", "NewApi")
    override fun initView() {
        toolBar?.setToolBarBg(resources.getColor(R.color.green_shen))
        toolBar?.setToolBarTitleBg(resources.getColor(R.color.white))
        setTitleContent(titleName = "Web browsing")
        ImmersionBar.with(this).statusBarColor(R.color.green_shen).init()
        bindingView.lifecycleOwner = this

        bindingView.x5WebView.settings.apply {
            javaScriptEnabled = true
            javaScriptCanOpenWindowsAutomatically = true
            setSupportZoom(true)
            displayZoomControls = false
            allowContentAccess = true
            domStorageEnabled = true
            mediaPlaybackRequiresUserGesture = true
            loadsImagesAutomatically = true

        }
    }

    override fun loadData(isRefresh: Boolean) {
        if (h5url.isEmpty()) {
            h5url = intent?.getStringExtra("h5url") ?: ""
        }
        bindingView.x5WebView.loadUrl(h5url)
    }

}