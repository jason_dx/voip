package com.ultralab.ultracall.activity.user.viewmodel

import androidx.lifecycle.MutableLiveData
import com.ultralab.base_lib.base.BaseViewMode
import com.ultralab.base_lib.tool.init

/**
 * @ClassName: ShowFairyTaleViewModel
 * @Description: 类作用描述
 * @Author: jason@leanktech.com
 * @Date: 2021/9/2 14:43
 */
class ShowFairyTaleViewModel: BaseViewMode() {

    /**
     * 关闭麦克风
     */
    val closeMute = MutableLiveData<Boolean>().init(false)

    /**
     * 关闭扬声器
     */
    val closeSpeaker = MutableLiveData<Boolean>().init(false)

    /**
     * 区号
     */
    val areaCodeStr = MutableLiveData<String>().init("")

    /**
     * 手机号
     */
    val phoneNumberStr = MutableLiveData<String>().init("")

    /**
     * 当前通话剩余时间
     */
    val remainTime = MutableLiveData<String>().init("")

}