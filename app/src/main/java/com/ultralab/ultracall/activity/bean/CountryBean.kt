package com.ultralab.ultracall.activity.bean

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.ultralab.ultracall.BR
import java.io.Serializable

/**
 * @ClassName: CountryBean
 * @Description: 类作用描述
 * @Author: jason@leanktech.com
 * @Date: 2021/9/8 17:15
 */
class CountryBean constructor(map: HashMap<String, Any?>) : BaseObservable(),
    Serializable {
    val cname: String = map["cname"].toString()
    val cflag: String = map["cflag"].toString()
    val ccode: String = map["ccode"].toString()

    //是否选中当前的item
    @set:Bindable
    var checkItem: Boolean = false
        @Bindable get
        set(checkItem) {
            field = checkItem
            notifyPropertyChanged(BR.checkItem)
        }
}