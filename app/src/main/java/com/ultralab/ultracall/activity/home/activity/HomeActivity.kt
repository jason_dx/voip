package com.ultralab.ultracall.activity.home.activity

import android.content.Intent
import android.view.KeyEvent
import android.view.View
import android.view.WindowManager
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.alibaba.android.arouter.facade.annotation.Route
import com.google.android.gms.ads.identifier.AdvertisingIdClient
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.gyf.immersionbar.ImmersionBar
import com.jeremyliao.liveeventbus.LiveEventBus
import com.ultralab.base_lib.activity.BaseActivity
import com.ultralab.base_lib.tool.*
import com.ultralab.ultracall.LiveEventData
import com.ultralab.ultracall.R
import com.ultralab.ultracall.activity.home.fragment.CallFragment
import com.ultralab.ultracall.activity.home.viewmodel.HomeViewModel
import com.ultralab.ultracall.activity.user.activity.SettingActivity
import com.ultralab.ultracall.activity.user.fragment.MineFragment
import com.ultralab.ultracall.conversation.CallUtils
import com.ultralab.ultracall.databinding.HomeActivityBinding
import com.ultralab.ultracall.local.AccountBean
import kotlinx.coroutines.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.IOException


/**
 * @ClassName: MainActivity
 * @Description: 主页面
 * @Author: jason@leanktech.com
 * @Date: 2021/8/20 15:51
 */
@Route(path = "app/home")
class HomeActivity : BaseActivity<HomeActivityBinding>() {

    companion object {
        /**
         * 切换首页
         */
        const val CHECK_CALL = 0

        /**
         * 切换我的
         */
        const val CHECK_MINE = 1
    }

    private val mViewModel by viewModel<HomeViewModel>()

    private val fragmentList = mutableListOf<Fragment>()

    private var oldFragmentIndex = -1

    override val showToolBar: Boolean = false

    /**
     * 二次点击退出计时
     */
    private var mExitTime: Long = 0

    override fun getLayoutId(): Int = R.layout.home_activity

    override fun initView() {
        // These flags ensure that the activity can be launched when the screen is locked.
        val window = window
        window.addFlags(
            WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                    or WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                    or WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
        )
        //设置状态栏高度
        bindingView.statusBar.layoutParams.height =
            StatusBarTool.getStatusBarHeight(bindingView.statusBar.context)

        ImmersionBar.with(this).transparentBar().init()

        bindingView.presenter = this
        bindingView.lifecycleOwner = this
        bindingView.vm = mViewModel
        bindingView.ab = AccountBean

        //设置menu宽度
        val layoutParams = bindingView.openDrawerLayout.layoutParams
        layoutParams.width = (AppTool.getScreenWidth() * 0.7).toInt()
        bindingView.openDrawerLayout.layoutParams = layoutParams

        //设置drawer监听
        bindingView.drawerLayout.addDrawerListener(object : DrawerLayout.DrawerListener {
            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                //位置改变
            }

            override fun onDrawerOpened(drawerView: View) {
                //展开
            }

            override fun onDrawerClosed(drawerView: View) {
                //关闭
            }

            override fun onDrawerStateChanged(newState: Int) {
                //状态改变
            }
        })

        fragmentList.add(CallFragment())
        fragmentList.add(MineFragment())
        switchFragment(CHECK_CALL)
        //初始化call
        CallUtils.initCall(this)

        CallUtils.registerForCallInvites()

        CallUtils.handleIncomingCallIntent(intent)

        mViewModel.checkState.observe(this) {
            when (it) {
                0 -> {
                    bindingView.callIv.setImageResource(R.mipmap.bottom_tab_call_fill)
                    switchFragment(CHECK_CALL)
                }
                1 -> {
                    bindingView.mineIv.setImageResource(R.mipmap.bottom_tab_score)
                    switchFragment(CHECK_MINE)
                }
            }
            oldFragmentIndex = it
        }

        //监听注册状态
        LiveEventBus.get(LiveEventData.INIT_STATE, String::class.java).observe(this, {
            if (it == "20104" || it == "31400") {
                CallUtils.disconnect()
                //从新注册
                mViewModel.getTwilioToken {
                    CallUtils.registerForCallInvites()
                }
            }
        })
        //纠错逻辑
        //if (AccountBean.adCredits < 0) {
        //    AccountBean.adCredits = 0
        //}
//        if (AccountBean.adCredits == 0) {
//            AccountBean.adCredits = 1000
//        }

        getIdThread()
    }

    override fun loadData(isRefresh: Boolean) {

    }

    override fun onClick(v: View?) {
        super.onClick(v)
        if (v != null) {
            when (v.id) {
                R.id.call_click_view -> {
                    mViewModel.checkState.value = 0
                }
                R.id.credits_click_view -> {
                    mViewModel.checkState.value = 1
                }
                R.id.open_drawer_iv -> {
                    bindingView.drawerLayout.openDrawer(bindingView.openDrawerLayout)
                }
                //sign
                R.id.sign_click_area_view -> {
                    //startActivity(Intent(this, LoginActivity::class.java))
                }
                //credits
                R.id.credits_click_area_view -> {
                    //"点击".showToast()
                }
                //setting
                R.id.setting_click_area_view -> {
                    startActivity(Intent(this, SettingActivity::class.java))
                }
                //show google ad
                R.id.show_google_ad_tv -> {

                }
            }
        }
    }

    /**
     * fragment 切换
     */
    private fun switchFragment(nowIndex: Int) {
        val nowFragment: Fragment = fragmentList[nowIndex]

        val previousFragment: Fragment? = if (oldFragmentIndex == -1) {
            null
        } else {
            fragmentList[oldFragmentIndex]
        }

        supportFragmentManager.beginTransaction().apply {
            previousFragment?.let {
                hide(it)
            }
            if (!nowFragment.isAdded) {
                add(R.id.switch_frame_layout, nowFragment)
            } else {
                show(nowFragment)
            }
            commit()
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        intent?.let { CallUtils.handleIncomingCallIntent(it) }
    }


    override fun onResume() {
        super.onResume()
        CallUtils.registerReceiver()
    }

    override fun onDestroy() {
        CallUtils.unregisterReceiver()
        CallUtils.activityDes(this)
        super.onDestroy()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.action == KeyEvent.ACTION_DOWN) {
            if (System.currentTimeMillis() - mExitTime > 2000) {
                (resources.getString(R.string.exit_app) + resources.getString(R.string.app_name)).showToast()
                mExitTime = System.currentTimeMillis()
                return true
            }
            ActivityMgr.finishAllActivity()
        }
        return super.onKeyDown(keyCode, event)
    }

    /**
     * 获取 AdvertisingId
     */
    private fun getIdThread() {
        CoroutineScope(Dispatchers.IO).launch {
            var adInfo: AdvertisingIdClient.Info? = null
            try {
                adInfo = AdvertisingIdClient.getAdvertisingIdInfo(this@HomeActivity)
            } catch (e: IOException) {
                // Unrecoverable error connecting to Google Play services (e.g.,
                // the old version of the service doesn't support getting AdvertisingId).
            } catch (e: GooglePlayServicesNotAvailableException) {
                // Google Play services is not available entirely.
            }
            if (adInfo != null) {
                val id: String = adInfo.id
                val isLAT: Boolean = adInfo.isLimitAdTrackingEnabled
                "AdvertisingId id=$id\nisLat=$isLAT".print()
            }
        }
    }
}