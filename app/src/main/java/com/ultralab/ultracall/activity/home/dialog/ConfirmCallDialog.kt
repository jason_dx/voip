package com.ultralab.ultracall.activity.home.dialog

import android.Manifest
import android.app.AlertDialog
import android.view.Gravity
import android.view.View
import com.hjq.permissions.OnPermissionCallback
import com.hjq.permissions.XXPermissions
import com.ultralab.base_lib.fragment.ShadowDialogFragment
import com.ultralab.base_lib.tool.*
import com.ultralab.ultracall.ActivityCallParameter
import com.ultralab.ultracall.R
import com.ultralab.ultracall.activity.home.viewmodel.ConfirmCallViewModel
import com.ultralab.ultracall.databinding.ConfirmCallDialogBinding
import com.ultralab.ultracall.local.AccountBean
import com.ultralab.ultracall.tools.getAreaCreditsNumber
import com.ultralab.ultracall.tools.goToCall
import com.ultralab.ultracall.tools.timeStampToData
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * @ClassName: ConfirmCallDialog
 * @Description: 确认通话页面
 * @Author: jason@leanktech.com
 * @Date: 2021/9/6 14:36
 */
class ConfirmCallDialog : ShadowDialogFragment<ConfirmCallDialogBinding>() {

    private val mViewModel by viewModel<ConfirmCallViewModel>()

    /**
     * 区号
     */
    private val areaCode by lazy {
        arguments?.getString(ActivityCallParameter.CALL_AREA_CODE)
    }

    /**
     * 手机号
     */
    private val phoneNumber by lazy {
        arguments?.getString(ActivityCallParameter.CALL_PHONE_NUMBER)
    }

    /**
     * 地区名称
     */
    private val areaName by lazy {
        arguments?.getString(ActivityCallParameter.CALL_AREA_NAME) ?: "CHINA"
    }

    /**
     * 地区名称
     */
    private val areaImage by lazy {
        arguments?.getString(ActivityCallParameter.CALL_AREA_IMAGE) ?: "CHINA"
    }

    /**
     * 地区名称
     */
    private val saveDb by lazy {
        arguments?.getBoolean(ActivityCallParameter.CALL_IS_SAVE_DB) ?: true
    }

    /** 高度 */
    override var dialogHeight: Int = AppTool.showContentHeight -
            StatusBarTool.getStatusBarHeight(
                context ?: ActivityMgr.currentActivity()!!
            )

    /** dialog 弹出样式 */
    override val animStyle: Int = R.style.dialogBottomScaleAnim

    /** 位置 */
    override val gravity = Gravity.BOTTOM

    /** 回调 */
    var freeCallBack: ((Boolean) -> Unit)? = null

    override fun getLayoutId(): Int = R.layout.confirm_call_dialog

    override fun initView() {
        bindingView.vm = mViewModel
        bindingView.lifecycleOwner = this
        bindingView.ab = AccountBean
        bindingView.presenter = this
        mViewModel.areaCode.value = areaCode
        mViewModel.phoneNumber.value = phoneNumber
        mViewModel.areaName.value = areaName
        mViewModel.areaImage.value = areaImage

        val areaCreditsNumber = areaName.getAreaCreditsNumber()
        if (areaCreditsNumber.isNotNullEmpty()) {
            if (AccountBean.adCredits < areaCreditsNumber.toInt()) {
                mViewModel.remainTime.value = "0"
            } else {
                mViewModel.remainTime.value =
                    (AccountBean.adCredits / areaCreditsNumber.toInt())
                        .toString()
            }
            AccountBean.singleConsumption = areaCreditsNumber.toInt()
            mViewModel.singleConsumption.value = areaCreditsNumber.toInt()
        } else {
            mViewModel.remainTime.value =
                (AccountBean.adCredits / AccountBean.singleConsumption)
                    .toString()
        }
        //当前时间
        mViewModel.nowTimeStr.value = System.currentTimeMillis().toTime("HH:mm")
        //当前日期
        mViewModel.currentDateStr.value = System.currentTimeMillis().timeStampToData()
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        if (v != null) {
            AppTool.singleClick(v) {
                when (v.id) {
                    R.id.confirm_root_ly -> {
                        dismiss()
                    }
                    R.id.confirm_call_close_iv -> {
                        dismiss()
                    }
                    R.id.bottom_view -> {
                        //nothing
                    }
                    R.id.confirm_call_close_btn -> {
                        if (XXPermissions.isGranted(
                                requireContext(),
                                Manifest.permission.RECORD_AUDIO
                            )
                        ) {
                            gotoCall()
                        } else {
                            XXPermissions.with(requireContext())
                                .permission(Manifest.permission.RECORD_AUDIO)
                                .request(object : OnPermissionCallback {
                                    override fun onGranted(
                                        permissions: MutableList<String>?,
                                        all: Boolean
                                    ) {
                                        if (all) {
                                            gotoCall()
                                        }
                                    }

                                    override fun onDenied(
                                        permissions: MutableList<String>?,
                                        never: Boolean
                                    ) {
                                        if (never) {
                                            "Microphone permissions needed. Please allow in your application settings.".showToast()
                                            XXPermissions.startPermissionActivity(
                                                requireContext(),
                                                permissions
                                            )
                                        } else {
                                            "Microphone permissions needed. Please allow in your application settings.".showToast()
                                        }
                                    }
                                })
                        }
                    }
                }
            }
        }
    }


    private fun showErrorMessage(message: String) {
        //显示签到成功弹窗
        val builder = AlertDialog.Builder(this.requireContext())
        builder.setMessage(message)
        builder.setPositiveButton("OK") { dialog, _ ->
            dialog.dismiss()
        }
        builder.create().apply {
            setCancelable(false)
            setCanceledOnTouchOutside(false)
        }.show()
    }

    /**
     * 调用拨打电话逻辑
     */
    private fun gotoCall() {
        if (AccountBean.adCredits > 0) {
            if (mViewModel.singleConsumption.get(0) <= AccountBean.adCredits) {
                freeCallBack?.invoke(true)
                if (phoneNumber.isNotNullEmpty() && areaCode.isNotNullEmpty()) {
                    goToCall(
                        requireContext(),
                        phoneNumber ?: "",
                        areaCode ?: "",
                        areaName,
                        areaImage,
                        saveDb
                    )
                    //单次消耗积分
                    AccountBean.adCredits -= mViewModel.singleConsumption.get(0)
                }
                dismiss()
            } else {
                showErrorMessage(resources.getString(R.string.time_is_not_enough))
                ActivityMgr.finishFairyTableActivity()
            }
        } else {
            showErrorMessage(resources.getString(R.string.time_is_not_enough))
        }
    }
}