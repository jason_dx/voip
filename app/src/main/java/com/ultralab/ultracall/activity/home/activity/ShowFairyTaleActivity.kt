package com.ultralab.ultracall.activity.home.activity

import android.os.Handler
import android.os.SystemClock
import android.view.KeyEvent
import android.view.View
import com.gyf.immersionbar.BarHide
import com.gyf.immersionbar.ImmersionBar
import com.jeremyliao.liveeventbus.LiveEventBus
import com.ultralab.base_lib.activity.BaseActivity
import com.ultralab.base_lib.tool.*
import com.ultralab.ultracall.ActivityCallParameter
import com.ultralab.ultracall.LiveEventData
import com.ultralab.ultracall.R
import com.ultralab.ultracall.activity.bean.DialListBean
import com.ultralab.ultracall.activity.user.viewmodel.ShowFairyTaleViewModel
import com.ultralab.ultracall.conversation.CallUtils
import com.ultralab.ultracall.conversation.SoundPoolManager
import com.ultralab.ultracall.databinding.ShowFairyTaleActivityBinding
import com.ultralab.ultracall.db.DbManger
import com.ultralab.ultracall.local.AccountBean
import com.ultralab.ultracall.tools.getAreaCreditsNumber
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * @ClassName: ShowFairyTaleActivity
 * @Description: 显示通过页面
 * @Author: jason@leanktech.com
 * @Date: 2021/9/2 14:42
 */
class ShowFairyTaleActivity : BaseActivity<ShowFairyTaleActivityBinding>() {

    /** 区号 */
    private val mAreaCode by lazy {
        intent.getStringExtra(ActivityCallParameter.CALL_AREA_CODE)
    }

    /** 手机号码 */
    private val mPhoneNumber by lazy {
        intent.getStringExtra(ActivityCallParameter.CALL_PHONE_NUMBER)
    }

    /**
     * 地区名称
     */
    private val mAreaName by lazy {
        intent?.getStringExtra(ActivityCallParameter.CALL_AREA_NAME) ?: "CHINA"
    }

    /**
     * 地区名称
     */
    private val mAreaImage by lazy {
        intent?.getStringExtra(ActivityCallParameter.CALL_AREA_IMAGE) ?: "CHINA"
    }

    /**
     * 地区名称
     */
    private val saveDb by lazy {
        intent?.getBooleanExtra(ActivityCallParameter.CALL_IS_SAVE_DB, false) ?: false
    }

    /** 当前记录秒  以秒为单位 */
    private var thisSecond = 0
    private var firstCount = false

    /**
     * 倒计时
     * handler
     */
    private val handler = Handler()
    private val runnable: Runnable by lazy {
        object : Runnable {
            override fun run() {
                val areaCreditsNumber = mAreaName.getAreaCreditsNumber()
                //鞥该
                fun toCallTime(){
                    if (areaCreditsNumber.isNotNullEmpty()) {
                        if (AccountBean.adCredits < areaCreditsNumber.toInt()) {
                            mViewModel.remainTime.value = "0"
                        } else {
                            mViewModel.remainTime.value =
                                (AccountBean.adCredits / areaCreditsNumber.toInt())
                                    .toString()
                        }
                    } else {
                        mViewModel.remainTime.value =
                            (AccountBean.adCredits / AccountBean.singleConsumption)
                                .toString()
                    }
                }


                if (firstCount && thisSecond / 60 == 1) {
                    thisSecond = 0
                    if (AccountBean.singleConsumption <= AccountBean.adCredits) {
                        AccountBean.adCredits -= areaCreditsNumber.toInt()
                        //得出当前剩余时间
                        toCallTime()
                    } else {
                        ActivityMgr.finishFairyTableActivity()
                    }
                } else {
                    //第一次满足一分钟，在走减的逻辑
                    if (thisSecond == 60) {
                        firstCount = true
                        thisSecond = 0
                    }
                }
                //计算当前剩余时间
                toCallTime()
                "AccountBean.adCredits=${AccountBean.adCredits}\nthisSecond=$thisSecond".print()
                thisSecond++
                handler.postDelayed(this, 1000)
            }
        }
    }


    private val mViewModel by viewModel<ShowFairyTaleViewModel>()

    override fun getLayoutId(): Int = R.layout.show_fairy_tale_activity

    override val showToolBar: Boolean
        get() = false

    /** 主动断开链接 */
    private var checkActiveDisconnect = false

    override fun initView() {
        ImmersionBar.with(this).hideBar(BarHide.FLAG_HIDE_BAR).fullScreen(true).init()
        bindingView.vm = mViewModel
        bindingView.lifecycleOwner = this
        bindingView.presenter = this
        bindingView.ab = AccountBean
        mViewModel.areaCodeStr.value = mAreaCode
        mViewModel.phoneNumberStr.value = mPhoneNumber

        "当前通过对方是=$mAreaCode$mPhoneNumber".print()
        CallUtils.call(hashMapOf("to" to "$mAreaCode$mPhoneNumber"))
        //计算当前剩余时间
        mViewModel.remainTime.value =
            (AccountBean.adCredits / AccountBean.singleConsumption)
                .toString()
        //是否需要存入数据库
        if (saveDb) {
            CoroutineScope(Dispatchers.Main).launch {
                delay(1000)
                //更新数据库
                val callBean = DialListBean().apply {
                    areaCode = mAreaCode?.toInt() ?: 0
                    phoneNumber = mPhoneNumber
                    areaImage = mAreaImage
                    areaName = mAreaName
                    dialNowTime = System.currentTimeMillis()
                }
                DbManger.upDataUser(callBean)
            }
        }

        setCallUI()
        // 启动计时器
        handler.postDelayed(runnable, 1000)

    }

    override fun loadData(isRefresh: Boolean) {

    }

    override fun onClick(v: View?) {
        super.onClick(v)
        if (v != null) {
            AppTool.singleClick(v) {
                when (v.id) {
                    R.id.mute_click_area_view -> {
                        val mute = CallUtils.mute()
                        mViewModel.closeMute.value = mute
                    }
                    R.id.speaker_click_area_view -> {
                        if (CallUtils.checkCallActivate()) {
                            mViewModel.closeSpeaker.value =
                                CallUtils.openSpeak(mViewModel.closeSpeaker.value ?: false)

                        }
                    }
                    R.id.fab_call_end -> {
                        checkActiveDisconnect = true
                        val disconnect = CallUtils.disconnect()
                        if (disconnect) {
                            finish()
                        }
                    }
                    else -> {
                    }
                }
            }
        }
    }

    /**
     * 启动计时
     */
    private fun setCallUI() {
        bindingView.chronometerView.base = SystemClock.elapsedRealtime()
        val hour = ((SystemClock.elapsedRealtime() - bindingView.chronometerView.base) / 1000 / 60)
        bindingView.chronometerView.format = "0$hour:%s"
        bindingView.chronometerView.start()
    }

    /**
     * 停止
     */
    private fun resetUI() {
        bindingView.chronometerView.stop()
        "当前time=${bindingView.chronometerView.text}".printError()
    }

    /**
     * 监听音量键
     */
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return when (keyCode) {
            KeyEvent.KEYCODE_VOLUME_DOWN -> {
                "onKeyDown->KEYCODE_VOLUME_DOWN".print()
                SoundPoolManager.getInstance(ActivityMgr.getContext())?.setVolume(false)
                true
            }
            KeyEvent.KEYCODE_VOLUME_UP -> {
                "onKeyDown->KEYCODE_VOLUME_UP".print()
                SoundPoolManager.getInstance(ActivityMgr.getContext())?.setVolume(true)
                true
            }
            else -> super.onKeyDown(keyCode, event)
        }
    }

    /**
     * 监听音量键
     */
    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        return when (keyCode) {
            KeyEvent.KEYCODE_VOLUME_DOWN -> {
                "onKeyUp->KEYCODE_VOLUME_DOWN".print()
                true
            }
            KeyEvent.KEYCODE_VOLUME_UP -> {
                "onKeyUp->KEYCODE_VOLUME_UP".print()
                true
            }
            else -> super.onKeyUp(keyCode, event)
        }
    }

    override fun onStop() {
        super.onStop()
        resetUI()
    }

    override fun onDestroy() {
        if (!checkActiveDisconnect) {
            CallUtils.disconnect()
        }
        // 停止计时器
        handler.removeCallbacks(runnable)
        LiveEventBus.get<Boolean>(LiveEventData.UPDATE_LIST).post(true)
        super.onDestroy()
    }
}