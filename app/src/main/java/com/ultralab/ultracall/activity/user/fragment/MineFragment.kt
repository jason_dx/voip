package com.ultralab.ultracall.activity.user.fragment

import android.app.AlertDialog
import android.view.View
import com.ultralab.base_lib.fragment.BaseFragment
import com.ultralab.ultracall.R
import com.ultralab.ultracall.activity.user.viewmodel.MineViewModel
import com.ultralab.ultracall.databinding.MineFragmentBinding
import com.ultralab.ultracall.local.AccountBean
import com.ultralab.ultracall.tools.checkTodayCheckInBoolean
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * @ClassName: MineFragment
 * @Description: 我的页面
 * @Author: jason@leanktech.com
 * @Date: 2021/8/26 16:48
 */
class MineFragment : BaseFragment<MineFragmentBinding>() {

    private val mViewModel by viewModel<MineViewModel>()

    override fun getLayoutId(): Int = R.layout.mine_fragment

    override fun initView() {
        bindingView.vm = mViewModel
        bindingView.lifecycleOwner = this
        bindingView.presenter = this
        bindingView.ab = AccountBean
    }

    override fun loadData(isRefresh: Boolean) {

    }

    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                //买次数
                R.id.by_frequency_view -> {
                    //"点击".showToast()
                }
                //设置phone
                R.id.set_phone_number_view -> {
                    //"点击".showToast()
                }
                //签到
                R.id.check_in_view -> {
                    checkInFun()
                }
            }
        }
    }

    /**
     * 签到成功弹窗以及失败
     */
    private fun checkInFun() {
        val message: String
        if (!checkTodayCheckInBoolean()) {
            AccountBean.adCredits = AccountBean.adCredits + 500
            AccountBean.todayCheckInString = System.currentTimeMillis()
            message = resources.getString(R.string.sign_in_success)
        } else {
            message = resources.getString(R.string.today_check_in)
        }
        //显示签到成功弹窗
        val builder = AlertDialog.Builder(this.requireContext())
        builder.setMessage(message)
        builder.setPositiveButton("OK") { dialog, _ ->
            dialog.dismiss()
        }
        builder.create().apply {
            setCancelable(false)
            setCanceledOnTouchOutside(false)
        }.show()
    }
}