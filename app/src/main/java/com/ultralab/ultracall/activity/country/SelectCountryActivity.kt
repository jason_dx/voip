package com.ultralab.ultracall.activity.country

import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.gyf.immersionbar.ImmersionBar
import com.ultralab.base_lib.activity.BaseActivity
import com.ultralab.base_lib.adapter.ItemClickPresenter
import com.ultralab.base_lib.adapter.SingleTypeAdapter
import com.ultralab.base_lib.tool.AppTool
import com.ultralab.base_lib.tool.getSortKey
import com.ultralab.ultracall.R
import com.ultralab.ultracall.activity.bean.CountryBean
import com.ultralab.ultracall.databinding.SelectCountryActivityBinding
import com.ultralab.ultracall.local.AccountBean
import com.ultralab.weight_lib.SideLetterBar
import com.ultralab.weight_lib.itemdecoration.CountryItemDecoration
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * @ClassName: SelectCountryActivity
 * @Description: 选择地区电话区号
 * @Author: jason@leanktech.com
 * @Date: 2021/9/8 16:23
 */
class SelectCountryActivity : BaseActivity<SelectCountryActivityBinding>(),
    ItemClickPresenter<CountryBean> {

    private val mViewMode by viewModel<SelectCountryViewModel>()

    private val mAdapter by lazy {
        SingleTypeAdapter(this, R.layout.country_item, mViewMode.data).apply {
            itemPresenter = this@SelectCountryActivity
        }
    }

    override fun getLayoutId(): Int = R.layout.select_country_activity

    override fun initView() {
        //设置状态栏高度
        toolBar?.setToolBarBg(resources.getColor(R.color.green_shen))
        setTitleContent(titleName = "Select a country")
        toolBar?.setToolBarTitleBg(resources.getColor(R.color.white))
        ImmersionBar.with(this).statusBarColor(R.color.green_shen).init()

        bindingView.vm = mViewMode
        bindingView.lifecycleOwner = this
        bindingView.presenter = this

        bindingView.azView.setOverlay(bindingView.tvLetterOverlay)
        bindingView.azView.setOnLetterChangedListener(object :
            SideLetterBar.OnLetterChangedListener {
            override fun onLetterChanged(letter: String?) {
                //通过遍历数据拿到对应position,移动到指定position
                letter?.let {
                    for (i in mViewMode.data.indices) {
                        if (letter == mViewMode.data[i].cname.getSortKey()) {
                            val layoutManager =
                                bindingView.countryRv.layoutManager as LinearLayoutManager
                            layoutManager.scrollToPositionWithOffset(i, 0)
                            return@let
                        }
                    }
                }
            }
        })
    }

    override fun loadData(isRefresh: Boolean) {
        mViewMode.loadCountryData {
            bindingView.countryRv.apply {
                layoutManager = LinearLayoutManager(this@SelectCountryActivity)
                addItemDecoration(
                    CountryItemDecoration(
                        this@SelectCountryActivity,
                        mViewMode.topData
                    )
                )
                adapter = mAdapter
            }
        }
    }

    override fun onItemClick(v: View?, item: CountryBean) {
        if (v != null) {
            AppTool.singleClick(v) {
                when (v.id) {
                    R.id.country_item_ly -> {
                        mViewMode.selectItem(item)
                        AccountBean.lastSelectedCountry =
                            "${item.cflag},${item.cname},${item.ccode}"
                        CoroutineScope(Dispatchers.Main).launch {
                            finish()
                        }
                    }
                }
            }
        }
    }
}