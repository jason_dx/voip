package com.ultralab.ultracall.activity.home.viewmodel

import androidx.lifecycle.MutableLiveData
import com.ultralab.base_lib.base.BaseViewMode
import com.ultralab.base_lib.tool.init
import com.ultralab.base_lib.tool.print
import com.ultralab.ultracall.local.AccountBean
import com.ultralab.ultracall.model.HomeRepository

/**
 * @ClassName: HomeViewModel
 * @Description: 主页model
 * @Author: jason@leanktech.com
 * @Date: 2021/8/26 15:40
 */
class HomeViewModel constructor(private val repository: HomeRepository) : BaseViewMode() {

    /**
     * sdk 初始化状态
     */
    val initStateString = MutableLiveData<String>().init("")

    /**
     * 切换fragment 标记位
     * 0 = homeFragment
     */
    val checkState = MutableLiveData<Int>().init(0)

    /**
     * 用户默认头像
     */
    val userDefaultImage = MutableLiveData<String>().init("")

    /**
     * 获取token
     */
    fun getTwilioToken(callBack: (Boolean) -> Unit) {
        launchGo({
            val twilioToken = repository.getTwilioToken()
            AccountBean.twilioToken = twilioToken.toString()
            "token = $twilioToken".print("TAG")
            callBack(true)
        }, {
            "token error = $it".print("TAG")
            callBack(false)
        })
    }

}