package com.ultralab.ultracall.activity.bean

import androidx.databinding.BaseObservable
import com.ultralab.ultracall.local.AccountBean
import java.io.Serializable

/**
 * @ClassName: DialListBean
 * @Description: 存储通话记录列表
 * @Author: jason@leanktech.com
 * @Date: 2021/8/30 15:26
 */
class DialListBean : BaseObservable(), Serializable {
    //记录id
    val id = 0L

    //临时id
    var temporaryId = AccountBean.userId.toLong()

    //头像地址
    val userImage = ""

    //区号
    var areaCode = 1

    //国家国旗
    var areaImage = ""

    //国家名称
    var areaName = ""

    //实际电话
    var phoneNumber: String? = "8004732255"

    //拨打时间
    var dialNowTime = 0L

    //拨打时长
    var callDuration = 0L

    //当前时间
    val createTime = System.currentTimeMillis()
}